vec4 effect(vec4 c, Image i, vec2 tc, vec2 sc)
{

	vec4 tmp = Texel(i, tc);

	number avg = ((tmp.r + tmp.g + tmp.b) / 3.) * (1. - c.a);

	//tmp.r = c.r * avg;
	//tmp.g = c.g * avg;
	//tmp.b = c.b * avg;

	tmp.r = (c.r * (c.a + avg)); // + tmp.r;
	tmp.g = (c.g * (c.a + avg)); // + tmp.g;
	tmp.b = (c.b * (c.a + avg)); // + tmp.b;

	return tmp;

}
