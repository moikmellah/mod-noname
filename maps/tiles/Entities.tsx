<?xml version="1.0" encoding="UTF-8"?>
<tileset name="Entities" tilewidth="16" tileheight="16" tilecount="64" columns="8">
 <image source="entityGuide.png" width="128" height="128"/>
 <tile id="0">
  <properties>
   <property name="actorType" value="defaultHero"/>
  </properties>
 </tile>
 <tile id="1">
  <properties>
   <property name="actorType" value="skeleton"/>
  </properties>
 </tile>
 <tile id="2">
  <properties>
   <property name="actorType" value="goblinPeasant"/>
  </properties>
 </tile>
 <tile id="3">
  <properties>
   <property name="actorType" value="goblinSamurai"/>
  </properties>
 </tile>
 <tile id="4">
  <properties>
   <property name="actorType" value="goblinAssassin"/>
  </properties>
 </tile>
 <tile id="5">
  <properties>
   <property name="actorType" value="goblinMage"/>
  </properties>
 </tile>
 <tile id="6">
  <properties>
   <property name="actorType" value="goblinLord"/>
  </properties>
 </tile>
 <tile id="7">
  <properties>
   <property name="actorType" value="goblinSoldier"/>
  </properties>
 </tile>
 <tile id="8">
  <properties>
   <property name="actorType" value="goblinGuard"/>
  </properties>
 </tile>
 <tile id="9">
  <properties>
   <property name="actorType" value="goblinKnight"/>
  </properties>
 </tile>
 <tile id="10">
  <properties>
   <property name="actorType" value="goblinCenturion"/>
  </properties>
 </tile>
 <tile id="11">
  <properties>
   <property name="actorType" value="goblinKing"/>
  </properties>
 </tile>
 <tile id="12">
  <properties>
   <property name="actorType" value="woodChest"/>
  </properties>
 </tile>
 <tile id="13">
  <properties>
   <property name="actorType" value="stoneChest"/>
  </properties>
 </tile>
 <tile id="14">
  <properties>
   <property name="actorType" value="silverChest"/>
  </properties>
 </tile>
 <tile id="15">
  <properties>
   <property name="actorType" value="goldChest"/>
  </properties>
 </tile>
 <tile id="16">
  <properties>
   <property name="actorType" value="1"/>
  </properties>
 </tile>
 <tile id="17">
  <properties>
   <property name="actorType" value="2"/>
  </properties>
 </tile>
 <tile id="18">
  <properties>
   <property name="actorType" value="3"/>
  </properties>
 </tile>
 <tile id="19">
  <properties>
   <property name="actorType" value="4"/>
  </properties>
 </tile>
 <tile id="20">
  <properties>
   <property name="actorType" value="5"/>
  </properties>
 </tile>
 <tile id="21">
  <properties>
   <property name="actorType" value="6"/>
  </properties>
 </tile>
 <tile id="22">
  <properties>
   <property name="actorType" value="7"/>
  </properties>
 </tile>
 <tile id="23">
  <properties>
   <property name="actorType" value="8"/>
  </properties>
 </tile>
 <tile id="24">
  <properties>
   <property name="actorType" value="exit"/>
  </properties>
 </tile>
 <tile id="25">
  <properties>
   <property name="actorType" value="hudBar"/>
  </properties>
 </tile>
 <tile id="26">
  <properties>
   <property name="actorType" value="invIcon"/>
  </properties>
 </tile>
 <tile id="28">
  <properties>
   <property name="actorType" value="saveBook"/>
  </properties>
 </tile>
</tileset>
