<?xml version="1.0" encoding="UTF-8"?>
<tileset name="bricks" tilewidth="16" tileheight="16" tilecount="121" columns="11">
 <image source="bricks.brown.png" width="176" height="176"/>
 <tile id="0">
  <properties>
   <property name="coll" value="15"/>
  </properties>
 </tile>
 <tile id="1">
  <properties>
   <property name="coll" value="15"/>
  </properties>
 </tile>
 <tile id="2">
  <properties>
   <property name="coll" value="15"/>
  </properties>
 </tile>
 <tile id="3">
  <properties>
   <property name="coll" value="15"/>
  </properties>
 </tile>
 <tile id="4">
  <properties>
   <property name="coll" value="15"/>
  </properties>
 </tile>
 <tile id="5">
  <properties>
   <property name="coll" value="15"/>
  </properties>
 </tile>
 <tile id="6">
  <properties>
   <property name="coll" value="15"/>
  </properties>
 </tile>
 <tile id="7">
  <properties>
   <property name="coll" value="15"/>
  </properties>
 </tile>
 <tile id="8">
  <properties>
   <property name="coll" value="15"/>
  </properties>
 </tile>
 <tile id="9">
  <properties>
   <property name="coll" value="15"/>
  </properties>
 </tile>
 <tile id="10">
  <properties>
   <property name="coll" value="15"/>
  </properties>
 </tile>
 <tile id="11">
  <properties>
   <property name="coll" value="15"/>
  </properties>
 </tile>
 <tile id="12">
  <properties>
   <property name="coll" value="15"/>
  </properties>
 </tile>
 <tile id="13">
  <properties>
   <property name="coll" value="15"/>
  </properties>
 </tile>
 <tile id="14">
  <properties>
   <property name="coll" value="15"/>
  </properties>
 </tile>
 <tile id="15">
  <properties>
   <property name="coll" value="15"/>
  </properties>
 </tile>
 <tile id="16">
  <properties>
   <property name="coll" value="15"/>
  </properties>
 </tile>
 <tile id="17">
  <properties>
   <property name="coll" value="15"/>
  </properties>
 </tile>
 <tile id="19">
  <properties>
   <property name="coll" value="15"/>
  </properties>
 </tile>
 <tile id="20">
  <properties>
   <property name="coll" value="0"/>
  </properties>
 </tile>
 <tile id="21">
  <properties>
   <property name="coll" value="15"/>
  </properties>
 </tile>
 <tile id="22">
  <properties>
   <property name="coll" value="15"/>
  </properties>
 </tile>
 <tile id="23">
  <properties>
   <property name="coll" value="15"/>
  </properties>
 </tile>
 <tile id="24">
  <properties>
   <property name="coll" value="15"/>
  </properties>
 </tile>
 <tile id="25">
  <properties>
   <property name="coll" value="15"/>
  </properties>
 </tile>
 <tile id="26">
  <properties>
   <property name="coll" value="15"/>
  </properties>
 </tile>
 <tile id="27">
  <properties>
   <property name="coll" value="15"/>
  </properties>
 </tile>
 <tile id="28">
  <properties>
   <property name="coll" value="15"/>
  </properties>
 </tile>
 <tile id="30">
  <properties>
   <property name="coll" value="15"/>
  </properties>
 </tile>
 <tile id="31">
  <properties>
   <property name="coll" value="15"/>
  </properties>
 </tile>
 <tile id="32">
  <properties>
   <property name="coll" value="15"/>
  </properties>
 </tile>
 <tile id="33">
  <properties>
   <property name="coll" value="1"/>
  </properties>
 </tile>
 <tile id="34">
  <properties>
   <property name="coll" value="1"/>
  </properties>
 </tile>
 <tile id="35">
  <properties>
   <property name="coll" value="1"/>
  </properties>
 </tile>
 <tile id="52">
  <properties>
   <property name="coll" value="1"/>
  </properties>
 </tile>
 <tile id="68">
  <properties>
   <property name="coll" value="1"/>
  </properties>
 </tile>
 <tile id="69">
  <properties>
   <property name="coll" value="1"/>
  </properties>
 </tile>
 <tile id="70">
  <properties>
   <property name="coll" value="1"/>
  </properties>
 </tile>
 <tile id="80">
  <properties>
   <property name="coll" value="1"/>
  </properties>
 </tile>
 <tile id="104">
  <properties>
   <property name="coll" value="1"/>
  </properties>
 </tile>
 <tile id="105">
  <properties>
   <property name="coll" value="1"/>
  </properties>
 </tile>
 <tile id="106">
  <properties>
   <property name="coll" value="1"/>
  </properties>
 </tile>
</tileset>
