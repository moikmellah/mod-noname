--[[
Copyright 2016 MoikMellah

This file is part of the NoName sample module for use with the SYPHA Engine.

The NoName module is free software: you can redistribute it and/or modify it under the terms
of the CC-By-SA 4.0 license as published by Creative Commons.

This code is provided strictly as-in and WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the included file 'LICENSE.CC-BY-SA.txt' for full details.

You should have received a copy of the CC-By-SA 4.0 License along with
the NoName module. If not, see https://creativecommons.org/licenses/by-sa/4.0/
]]--

require('utils.xml')

-- Table declaration for class
ProcGenLibrary = {}

-- Metatable for class (to make ProcGenLibrary table the default lookup for class methods)
ProcGenLibrary_mt = {}
ProcGenLibrary_mt.__index = ProcGenLibrary

-- Constructor
function ProcGenLibrary.new(randSeed)

	-- Make copy from template if provided, or default template if not
	local nActLib = {}

	-- Set metatable to get our class methods
	setmetatable(nActLib, ProcGenLibrary_mt)

	nActLib.mods = {}
	nActLib.count = 0

	nActLib.rseed = randSeed or 1
	nActLib.incr = 0

	nActLib.template = Xml.deserializeFile(gconf.modulePath .. '/scenes/scene-template.xml')
	local listXml = Xml.deserializeFile(gconf.modulePath .. '/scenes/enemyLists.xml')
	nActLib.enemyLists = listXml.enemyList

	return nActLib

end

function ProcGenLibrary:tryCreateDungeon(pathLen, branchMin, branchMax, nodeChance, ascChance, lvRate, savenum, initLevel)

	local nc = nodeChance or .5
	local ac = ascChance or .4

	local sn = savenum or 1

	local collection = {}
	
	local iter = 0
	local maxRetries = 3
	
	while ((#collection < pathLen*2) and (iter <= maxRetries)) do
		collection = self:createDungeon(pathLen, branchMin, branchMax, nc, ac, lvRate, savenum, initLevel)
		debugPrint('seed:'..self.rseed..'; count:'..#collection, 1)
		self.rseed = self.rseed + 1
		self.incr = 0
		iter = iter + 1
		--nc = nc + .01
	end

	local minimap = MinimapLibrary:new()
	for k,v in pairs(collection) do
		minimap:addRoom(v)
	end

	local fdat = minimap:composeMap(false, 8, 6)
	minimap:saveMapData()
	if(fdat == nil) then debugPrint('Did\'nt encode.', 2) end
	
	self:exportSceneFiles(collection, sn)
	
end

function ProcGenLibrary:createDungeon(pathLen, branchMin, branchMax, nodeChance, ascChance, lvRate, savenum, initLevel)

	local initLv = initLevel or 1

	local sn = savenum or 1

	local nch = nodeChance or 0.25

	local mQuery = self:getAvailableMaps('stock')
	local sQuery = self:getAvailableMaps('save')
	
	local mapQuery = mQuery
	
	local defDoor = {d = 0, p = 99, name = 'default', t = 3}
	
	local saveFreq = gconf.saveFreq or 5

	local pitch = pathLen*6

	local buffer = {}

	local stack = {}

	local distRemaining = pathLen

	local startX = pathLen*3

	local startY = pathLen*3
	
	local collection = {}
	
	local firstRoom = {}
	firstRoom.id = #collection+1
	firstRoom.map = mapQuery:First('x => x.file == "stock.1101b.tmx"')
	firstRoom.mapFile = firstRoom.map.file
	firstRoom.x = startX
	firstRoom.y = startY
	firstRoom.w = firstRoom.map.w
	firstRoom.h = firstRoom.map.h
	firstRoom.dw = firstRoom.map.doorsW
	firstRoom.de = firstRoom.map.doorsE
	firstRoom.level = initLv
	firstRoom.color = {r=50, g=255, b=50, a=255}
	
	table.insert(collection, firstRoom)
	
	buffer[startY * pitch + startX] = {rm = firstRoom, e = 1}
	
	local asc = self:getRandom() < .5
	
	table.insert(stack, {d = distRemaining, 
				p = self:getRandomRange(branchMin, branchMax),
				c = buffer[startY * pitch + startX], 
				t = 1,
				x = startX, 
				y = startY,
				maxx = 3,
				maxy = 3, 
				lv = initLv,
				dir = 'e',
				pos = 1,
				pAsc = asc,
				nodeRand = 99,
				ascRand = self:getRandom(),
				asc = asc})
				
	local keepGoing = true
	
	while ((#stack > 0) and keepGoing) do
		
		-- Grab an empty door and get to work.
		--local door = table.remove(stack)
		--New idea: Prioritize by type.  PITA, but hopefully gives better layouts.
		local dQuery = Queryable.new(stack)
		local door = dQuery:OrderBy('x => x.t'):First()
		for k = #stack,1,-1 do
			if(tostring(stack[k]) == tostring(door)) then table.remove(stack,k) break end
		end
		
		debugPrint('Path remaining: '..door.d, 3)
		
		-- Gather coords
		local y = door.y
		local xoffset = 1
		local connect = 'w'
		if(door.dir == 'w') then xoffset = -1 connect = 'e' end
		local x = door.x + xoffset
		
		local nxDoorPos = door.c.rm.map.h
		local yoffset = -1
		
		local arr = 'x.doors.' .. connect
		local doorWhich = '#' .. arr
		local doorWhere = '2 ^ (x.h - 1)'
		
		door.nxAsc = door.asc
		
		if(door.asc ~= true) then 
			doorWhich = '1'
			doorWhere = '1'
			yoffset = 1 
		end
		
		--mapQuery = mQuery
		if(door.t == 3) then
			saveFreq = saveFreq - 1
			if(saveFreq == 0) then 
				mapQuery = sQuery 
				saveFreq = 5
			else 
				mapQuery = mQuery 
			end
		else
			mapQuery = mQuery
		end

		-- Needs a door in the correct position.
		--local query = mapQuery:Where('x => '..arr..'['..doorWhich..'] == ' .. doorWhere)
		local query = mapQuery:Where('x => (bit.band(x.doors' .. string.upper(connect) .. ', ' .. doorWhere .. ') ~= 0)')
		
		-- Restrict based on max size
		query = query:Where('x => x.w <= '..door.maxx..' and x.h <= '..door.maxy)
		
		-- Determine total num of doors needed.
		local ds = 1
		
		local exits = {}
		
		-- Only if we're not at an endpoint
		if(door.d > 0 and door.t ~= 3) then
		
			local nxpath = door.p-1

			-- Add a door if we're pathing, and reset pathlen
			if(door.p <= 0) then
				nxpath = self:getRandomRange(branchMin, branchMax)
				ds = ds + 1
				table.insert(exits, 
						--self:getRandomRange(1,(#exits+1)),
						{name = 'branch', 
							d = (door.d-1), 
							lv = door.lv,
							p = self:getRandomRange(branchMin, branchMax),
							t = 2,
							ascRand = self:getRandom(),
							nodeRand = self:getRandom()
						})
			end
		
			-- Add a door regardless - need to keep pathing
			ds = ds + 1
			table.insert(exits, 1,
					--self:getRandomRange(1,(#exits+1)), 
					{
						name = 'path', 
						d = (door.d-1), 
						lv = door.lv,
						p = nxpath,
						t = 1,
						ascRand = self:getRandom(),
						nodeRand = self:getRandom()
					}
				)

			-- Randomly add a door for a node
			if(door.nodeRand < nch) then
				ds = ds + 1
				table.insert(exits, 
						--self:getRandomRange(1,(#exits+1)),
						{name = 'node', 
							d = 0, 
							lv = door.lv,
							p = 99,
							t = 3,
							ascRand = 99,
							nodeRand = 99
						})
			end
			
		end
		
		-- Shouldn't have more doors than walls for it
		ds = math.min(ds, door.maxx * door.maxy)
		query = query:Where('x => (#x.doors.e + #x.doors.w) == ' .. ds)
		
		-- Try biasing toward horizontal when not branching, maybe?
		if((ds == 2) and (door.ascRand > ascChance)) then
			query = query:Where('x => x.doorsE == x.doorsW')
			if(self:getRandom() > .5) then
				door.nxAsc = not(door.nxAsc)
			end
		end
		
		-- Finally, order by last usage
		query = query:OrderBy('x => x.lastUsage')
		
		local chosenRoom = query:First()
		
		if(chosenRoom ~= nil) then
			--chosenRoom.lastUsage = chosenRoom.lastUsage + self:getRandom()
			local rw = chosenRoom.w
			local rh = chosenRoom.h
			
			local clear = true
			
			local px = x+(xoffset*(rw-1))
			local py = y+(yoffset*(rh-1))
			
			local ll = math.min(x,px)
			local rr = math.max(x,px)
			local tt = math.min(y,py)
			local bb = math.max(y,py)

			-- Loop through and check that each spot is clear
			for ry = tt,bb do
				for rx = ll,rr do
					if(rx < 1 or rx > pitch-1 or ry < 1 or ry > pitch-1) then 
						clear = false 
						debugPrint(door.c.rm.map.file..':'..door.pos..door.dir..' out of bounds')
					elseif(buffer[ry*pitch + rx] ~= nil) then 
						clear = false 
						debugPrint(door.c.rm.map.file..':'..door.pos..door.dir..' overlap') 
					end
				end
				if(clear ~= true) then break end
				local doorPos = 2^(ry-tt)
				-- Check that doors aren't being blocked, too (except entry door).
				local tspace = buffer[ry*pitch+ll-1]
				if(tspace ~= nil and
				 	(tspace.e == 1 or bit.band(chosenRoom.doorsW, doorPos) ~= 0) and
				 	(ry ~= y or (ll-1) ~= door.x)
				 ) then 
				 	clear = false 
				 	debugPrint(door.c.rm.map.file..':'..door.pos..door.dir..' blocked or blocking.')
				 	break
				end
				
				-- Don't allow the next room to block a door
				local ttspace = buffer[ry*pitch+ll-2]
				if(ttspace ~= nil 
					and ttspace.e == 1 
					and (ry ~= y or (ll-1) ~= door.x) 
					and bit.band(chosenRoom.doorsW, doorPos) ~= 0
				) then 
				 	clear = false 
				 	debugPrint(door.c.rm.map.file..':'..door.pos..door.dir..' bridging segments.')
				 	break
				end

				local lspace = buffer[ry*pitch+rr+1]
				if(lspace ~= nil and 
					(lspace.w == 1 or bit.band(chosenRoom.doorsE, doorPos) ~= 0) and
					(ry ~= y or (rr+1) ~= door.x)
				) then 
					clear = false 
					debugPrint(door.c.rm.map.file..':'..door.pos..door.dir..' blocked or blocking.')
					break
				end

				-- Don't allow the next room to block a door
				local llspace = buffer[ry*pitch+rr+2]
				if(llspace ~= nil and 
					llspace.w == 1 and 
					(ry ~= y or (rr+1) ~= door.x)
					and bit.band(chosenRoom.doorsE, doorPos) ~= 0
				) then 
				 	clear = false 
				 	debugPrint(door.c.rm.map.file..':'..door.pos..door.dir..' bridging segments.')
				 	break
				end

			end
			
			-- If we failed for any reason above, reduce room size and try again.
			if(clear == false) then
				if(door.asc ~= door.pAsc) then
					if(door.maxx <= door.maxy) then
						door.maxy = door.maxy - 1
					else
						door.maxx = door.maxx - 1
					end
				end
				door.asc = not(door.asc)
				-- Pop it back on and try again.
				table.insert(stack,door)
			else
				chosenRoom.lastUsage = chosenRoom.lastUsage + 1 + self:getRandom()
				local doorVal = 1
				if (door.asc == true) then
					doorVal = 2 ^ (chosenRoom.h - 1)
				end
				--All clear.  Create it, link it to the previous door, and fill the buffer.
				local newRoom = {
					id = #collection+1, 
					map = chosenRoom, 
					mapFile = chosenRoom.file,
					doors = { [doorVal..connect] = door.c.rm.id..'.'..door.pos..door.dir },
					x = ll,
					y = tt,
					level = math.floor(door.lv),
					w = chosenRoom.w,
					h = chosenRoom.h,
					dw = chosenRoom.doorsW,
					de = chosenRoom.doorsE,
					color = chosenRoom.color
					}
				table.insert(collection, newRoom)
				
				door.c.rm.doors = door.c.rm.doors or {}
				door.c.rm.doors[door.pos..door.dir] = newRoom.id..'.'..doorVal..connect
				
				debugPrint(newRoom.id..': ('..door.c.rm.id..')'..door.c.rm.map.file..':'..door.pos..door.dir..'->'..newRoom.map.file..':'..doorVal..connect, 2)
				
				for bx = ll,rr do

					local ttbb = 1
					local ltt = tt
					local lbb = bb
			
					if(love.math.random() > .5) then
						ltt = bb
						lbb = tt
						ttbb = -1
					end

					for by = ltt,lbb,ttbb do
						-- Don't include the door we came in.
						local dont = ''
						if(bx == x and by == y) then
							dont = connect
						end
						-- Fill the buffer
						buffer[by*pitch+bx] = {rm = newRoom}
						local doorPos = 2^(by-tt)
						-- Add doors to the stack as appropriate
						if(bx == ll and bit.band(newRoom.map.doorsW, doorPos) ~= 0 and dont ~= 'w') then
							local rem = 1
							if(connect == 'w') then rem = nil end
							buffer[by*pitch+bx].w = 1
							local doorSpec = table.remove(exits,rem) or defDoor
							local newDoor = {d = doorSpec.d, 
								p = doorSpec.p,
								lv = doorSpec.lv + lvRate,
								c = buffer[by*pitch+bx], 
								t = doorSpec.t,
								nodeRand = doorSpec.nodeRand,
								ascRand = doorSpec.ascRand,
								x = bx, 
								y = by,
								maxx = 3,
								maxy = 3, 
								dir = 'w',
								pos = doorPos,
								pAsc = door.nxAsc,
								asc = door.nxAsc}
							table.insert(stack, newDoor)
						end
						if(bx == rr and bit.band(newRoom.map.doorsE, doorPos) ~= 0 and dont ~= 'e') then
							local rem = 1
							if(connect == 'e') then rem = nil end
							buffer[by*pitch+bx].e = 1
							local doorSpec = table.remove(exits,rem) or defDoor
							local newDoor = {d = doorSpec.d, 
								p = doorSpec.p,
								lv = doorSpec.lv + lvRate,
								c = buffer[by*pitch+bx], 
								t = doorSpec.t,
								nodeRand = doorSpec.nodeRand,
								ascRand = doorSpec.ascRand,
								x = bx, 
								y = by,
								maxx = 3,
								maxy = 3, 
								dir = 'e',
								pos = doorPos,
								pAsc = door.nxAsc,
								asc = door.nxAsc}
							table.insert(stack, newDoor)
						end
					end
				end
			end
			
		elseif(door.maxx * door.maxy > 0) then
			-- Push and retry.
			table.insert(stack, door)
		else
			debugPrint('Map creation failed.', 1)
			keepGoing = false
			local brokenRoom = {id = #collection+1, 
					x = x,
					y = y,
					w = 1,
					h = 1,
					dw = 0,
					de = 0,
					color = {r=100,g=100,b=100,a=255}}
			table.insert(collection,brokenRoom)
		end
	end

	return collection

end

function ProcGenLibrary:getRandom()
--	self.incr = ((self.incr or 0) + .001) % 1
	self.incr = (self.incr or 0) + 1
	local retVal = love.math.noise(self.incr, self.rseed)	
	return retVal
end

function ProcGenLibrary:getRandomRange(min, max)

	local n = min or 0
	local x = max or 1
	
	local diff = x - n
	
	local randVal = math.floor(self:getRandom() * diff + .5) + n
	
	return randVal
	
end

function ProcGenLibrary:exportSceneFiles(roomList, savenum)

	local sn = savenum or 1
	
	local savepath = gconf.modulePath .. '/scenes/' .. sn .. '/'
	
	love.filesystem.createDirectory(savepath)
	
	local listQuery = Queryable.new(self.enemyLists)
	
	for k,v in pairs(roomList) do
	
		local tCopy = tableCopy(self.template)
		tCopy.vars.sceneId = k
		tCopy.vars.level = v.level
		
		tCopy.tilemap[0] = '../../maps/' .. v.mapFile

		local list = listQuery:Where('x => x.env == ' .. (v.env or 1)):ToArray()
		
		tCopy.actor = list[self:getRandomRange(1, #list)].actor
		
		tCopy.exit = v.doors
		
		local tString = Xml.serialize(tCopy, 'sceneConfig', 'sceneConfig')
		
		-- Write!
		local tFile = love.filesystem.newFile(savepath .. '/' .. k .. '.xml')
		tFile:open("w")
		tFile:write(tString)
		tFile:flush()
		tFile:close()
	
	end

end

function ProcGenLibrary:getAvailableMaps(fileSet)

	local tag = fileSet or 'stock'

	local path = pathToFile(gconf.modulePath .. '/maps/')

	local fullList = love.filesystem.getDirectoryItems(path)

	local trimList = Queryable.new(fullList):Where('x => string.match(x, "'..tag..'.") ~= nil'):ToArray()

	local availableMaps = {}
	
	-- Set up our map definitions
	for k,v in ipairs(trimList) do
		local tMap = {}
		tMap.path = path .. v
		tMap.file = v
		local ha,hb,w,h,doorsW,doorsE = string.find(v, '^'..tag..'%.([0-9])([0-9])([0-9])([0-9]).+%.tmx$')
		w = tonumber(w) or 1
		h = tonumber(h) or 1
		tMap.doorsW = tonumber(doorsW) or 0
		tMap.doorsE = tonumber(doorsE) or 0
		
		tMap.w = w
		tMap.h = h
		
		tMap.doors = { w = {}, e = {} }
		
		tMap.lastUsage = self:getRandom()
		
		if(tag == 'save') then
			tMap.color = {r=255,g=0,b=0,a=255}
		end
		
		for b = 0,3 do
			local p2 = 2^b
			if(bit.band(tMap.doorsW, p2) ~= 0) then table.insert(tMap.doors.w, p2) end
			if(bit.band(tMap.doorsE, p2) ~= 0) then table.insert(tMap.doors.e, p2) end
		end

		table.insert(availableMaps, tMap)
		
	end

	local mapQuery = Queryable.new(availableMaps)

	return mapQuery

end
