--[[
Copyright 2016 MoikMellah

This file is part of the NoName sample module for use with the SYPHA Engine.

The NoName module is free software: you can redistribute it and/or modify it under the terms
of the CC-By-SA 4.0 license as published by Creative Commons.

This code is provided strictly as-in and WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the included file 'LICENSE.CC-BY-SA.txt' for full details.

You should have received a copy of the CC-By-SA 4.0 License along with
the NoName module. If not, see https://creativecommons.org/licenses/by-sa/4.0/
]]--

require('utils.xml')

-- Table declaration for class
MinimapLibrary = {}

-- Metatable for class (to make MinimapLibrary table the default lookup for class methods)
MinimapLibrary_mt = {}
MinimapLibrary_mt.__index = MinimapLibrary

-- Constructor
function MinimapLibrary.new()

	-- Make copy from template if provided, or default template if not
	local nActLib = {}

	-- Set metatable to get our class methods
	setmetatable(nActLib, MinimapLibrary_mt)

	nActLib.rooms = {}
	nActLib.doors = 0
	nActLib.minx = nil
	nActLib.maxx = nil
	nActLib.miny = nil
	nActLib.maxy = nil

	return nActLib

end

function MinimapLibrary:getPitch()
	return self.maxx - self.minx
end

function MinimapLibrary:getHeight()
	return self.maxy - self.miny
end

function MinimapLibrary:addRoom(room)

	table.insert(self.rooms, room.id, room)
	
	self.minx = math.min(room.x, (self.minx or 9999999))
	self.miny = math.min(room.y, (self.miny or 9999999))
	self.maxx = math.max(room.x + room.w - 1, (self.maxx or -1))
	self.maxy = math.max(room.y + room.h - 1, (self.maxy or -1))
	
end

function MinimapLibrary:composeMap(hide, tileW, tileH, savenum, outputOnly)

	local sn = savenum or 'A'

	local w = tileW or 8
	local h = tileH or 8
	
	local pitch = self:getPitch()
	local height = self:getHeight()
	
	local defColor = {r=50,g=50,b=255,a=255}
	local curRmColor = {r=255,g=255,b=255,a=255}

	--debugPrint('Creating map image '..(pitch+3)*w..'x'..(height+3)*h, 2)

	-- Grab a canvas to paint on (with 1x padding all around)
	local canvas = love.graphics.newCanvas((pitch+3)*w, (height+3)*h)
	local origCanvas = love.graphics.getCanvas()
	love.graphics.setCanvas(canvas)
	love.graphics.setColor(255,255,255,255)
	love.graphics.clear(34,32,52,255)
	love.graphics.setColor(255,255,255,255)

	-- MAGIC
	for mk,mv in pairs(self.rooms) do
	
		if((hide ~= true) or mv.disc) then
			
			--debugPrint('Drawing room '..mv.id, 5)
			
			local rl = mv.x - self.minx + 1
			local rt = mv.y - self.miny + 1
			local rw = mv.w
			local rh = mv.h

			local c = mv.color or defColor
			if(mk == self.currentRoom) then
				c = curRmColor
			end

			-- Draw the room
			love.graphics.setColor(c.r, c.g, c.b, c.a)
			love.graphics.rectangle('fill', rl*w,rt*h, (rw*w)-1, (rh*h)-1)
			
			-- Draw the doors
			for dbit = 1,3 do
			
				-- West
				local dpos = 2^(dbit-1)
				local hasDoor = (bit.band(mv.dw, dpos) ~= 0)
				
				if(hasDoor) then

					local ll = (rl*w) - .5
					local lt = ((rt+dbit-1)*h) + 2
				
					-- Draw it!
					love.graphics.setColor(255,255,255,255)
					love.graphics.setLineWidth(1)
					love.graphics.setLineStyle('rough')
					love.graphics.line(ll,lt,ll,lt+h-5)
				
				end
				
				-- East
				hasDoor = (bit.band(mv.de, dpos) ~= 0)
				
				if(hasDoor) then

					local ll = ((rl+rw)*w) - .5
					local lt = ((rt+dbit-1)*h) + 2
				
					-- Draw it!
					love.graphics.setColor(255,255,255,255)
					love.graphics.setLineWidth(1)
					love.graphics.setLineStyle('rough')
					love.graphics.line(ll,lt,ll,lt+h-5)
				
				end
			
			end
		
		end
	
	end

	-- Cleanup
	love.graphics.setColor(255,255,255,255)
	love.graphics.setCanvas(origCanvas)

	-- Output!
	local imgData = canvas:newImageData()

	love.filesystem.createDirectory(gconf.modulePath .. '/maps/' .. sn)	
	if(outputOnly ~= true) then
		imgData:encode('png', gconf.modulePath .. '/maps/'..sn..'/mapImage.png')
	end
	
	return canvas
	
end

function MinimapLibrary:saveMapData(savePath)
	
	local sTab = {}
	for k,v in ipairs(self.rooms) do
		local n = {}
		n.id = v.id
		n.x = v.x
		n.y = v.y
		n.w = v.w
		n.h = v.h
		n.de = v.de
		n.dw = v.dw
		n.mapFile = v.mapFile
		n.disc = v.disc
		n.color = v.color
		
		table.insert(sTab, n.id, n)
	end
	
	local sStr = Xml.serialize(sTab, 'rooms', 'rooms')
	local path = savePath or (gconf.modulePath .. '/maps/A/mapData.xml')

	local tFile = love.filesystem.newFile(path)
	tFile:open("w")
	tFile:write(sStr)
	tFile:flush()
	tFile:close()	
	
end

function MinimapLibrary:loadMapData(savePath)

	local path = savePath or (gconf.modulePath .. '/maps/A/mapData.xml')

	local tbl = Xml.deserializeFile(path)
	
	for k,v in ipairs(tbl) do
		self:addRoom(v)
	end

end

function MinimapLibrary:discoverRoom(sceneId)

	local id = tonumber(sceneId) or 1

	local room = self.rooms[id]
	
	room.disc = 1
	self.currentRoom = id

end
