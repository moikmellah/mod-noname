--[[
Copyright 2016 MoikMellah

This file is part of the NoName sample module for use with the SYPHA Engine.

The NoName module is free software: you can redistribute it and/or modify it under the terms
of the CC-By-SA 4.0 license as published by Creative Commons.

This code is provided strictly as-in and WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the included file 'LICENSE.CC-BY-SA.txt' for full details.

You should have received a copy of the CC-By-SA 4.0 License along with
the NoName module. If not, see https://creativecommons.org/licenses/by-sa/4.0/
]]--

require('classes.Actor')
require('utils.Inheritance')

SceneObject = Inherit(nil, Actor)

function SceneObject:sparkle()

	self.prim = self.prim or {}

	self.vars.spDelay = self.vars.spDelay or .33
	
	self.vars.spTrack = (self.vars.spTrack or 0) + self.dt
	
	if(self.vars.spTrack > self.vars.spDelay) then
		self.vars.spTrack = self.vars.spTrack - self.vars.spDelay
		local newPrim = {}
		newPrim.type = 'pointSet'
		newPrim.visible = 1
		newPrim.x = love.math.random(0,16)
		newPrim.y = love.math.random(8,16)*-1
		newPrim.point = {[1]={x=0,y=0}}
		newPrim.color = tableCopy(self.vars.color or {r = 1, g = 1, b = 1})
		newPrim.color.a = 1
		table.insert(self.prim,newPrim)
	end
	
	for i = #self.prim,1,-1 do
		local v = self.prim[i]
	
		v.y = v.y - (self.dt * 16)
		v.color.a = v.color.a - (self.dt/2)
		if(v.color.a < 0) then
			table.remove(self.prim, i)
		end
	
	end

end

function SceneObject:glow()
	
	self.vars.color = self.vars.color or {r=1,g=1,b=1,a=1,shader='tint'}
	self.vars.glowDir = self.vars.glowDir or -1
	
	self.vars.color.a = self.vars.color.a + (self.vars.glowDir * self.dt / 2)
	
	if(self.vars.color.a < 0) then
		self.vars.color.a = 0
		self.vars.glowDir = 1
	elseif(self.vars.color.a > .75) then
		self.vars.color.a = .75
		self.vars.glowDir = -1
	end	
end

function SceneObject:saveGame()

	local heroTable = {}
	heroTable.inv = self.scene.hero.inv
	heroTable.xp = self.scene.hero.base.xp
	heroTable.classList = self.scene.hero.base.classList
	heroTable.skills = self.scene.hero.base.skills
	
	local hString = Xml.serialize(heroTable, 'heroData', 'heroData')
	
	
	-- Write!
	local tFile = love.filesystem.newFile('save1/heroData.xml')
	tFile:open("w")
	tFile:write(hString)
	tFile:flush()
	tFile:close()

	local saveTable = {}
	saveTable.flags = self.scene.env.flags
	saveTable.scene = self.scene.sceneFile
	
	local sString = Xml.serialize(saveTable, 'gameData', 'gameData')
	
	-- Write!
	local tFile = love.filesystem.newFile('save1/gameData.xml')
	tFile:open("w")
	tFile:write(sString)
	tFile:flush()
	tFile:close()
	
	self.scene.env.vars.mapLib:saveMapData('save1/mapData.xml')

end
