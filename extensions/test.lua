local fct = 1.025

--local cls = {hp = 8, mp = 2, str = 1.5, con = 1, int = .5, wis = .5, agi = .9, lck = .6}
local cls = {hp = 5.5, mp = 4.5, str = .5, con = .7, int = 1.5, wis = 1.2, agi = .5, lck = .6}

print ("LV\tHP\tMP\tSTR\tCON\tINT\tWIS\tAGI\tLCK\n")

local hdr = "BASE\t"
hdr = hdr .. cls.hp .. "\t"
hdr = hdr .. cls.mp .. "\t"
hdr = hdr .. cls.str .. "\t"
hdr = hdr .. cls.con .. "\t"
hdr = hdr .. cls.int .. "\t"
hdr = hdr .. cls.wis .. "\t"
hdr = hdr .. cls.agi .. "\t"
hdr = hdr .. cls.lck .. "\n"

print (hdr)

for y = 1,20 do

	local x = y+4

	local a = (fct^(x-1))*x

	--print(math.floor(a*10)/10)
	--print(a)
	local out = y .. "\t"
	out = out .. math.ceil(cls.hp * a) .. "\t"
	out = out .. math.ceil(cls.mp * a) .. "\t"
	out = out .. math.ceil(cls.str * a) .. "\t"
	out = out .. math.ceil(cls.con * a) .. "\t"
	out = out .. math.ceil(cls.int * a) .. "\t"
	out = out .. math.ceil(cls.wis * a) .. "\t"
	out = out .. math.ceil(cls.agi * a) .. "\t"
	out = out .. math.ceil(cls.lck * a) .. "\t"

	print(out .. "\n")

end
