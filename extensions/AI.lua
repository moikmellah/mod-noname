--[[
Copyright 2016 MoikMellah

This file is part of the NoName sample module for use with the SYPHA Engine.

The NoName module is free software: you can redistribute it and/or modify it under the terms
of the CC-By-SA 4.0 license as published by Creative Commons.

This code is provided strictly as-in and WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the included file 'LICENSE.CC-BY-SA.txt' for full details.

You should have received a copy of the CC-By-SA 4.0 License along with
the NoName module. If not, see https://creativecommons.org/licenses/by-sa/4.0/
]]--

require('utils.xml')

AI = AI or {}

-- This is a second layer of state machine, on top of the original.  I guess.
function AI.goblinPatrol(self, dt)

	self.vars.walkadj = .5

	self.vars.aiTimer = (self.vars.aiTimer or 0) - (dt or 0)
	self.vars.atkTimer = (self.vars.atkTimer or 0) - (dt or 0)
	
	if(self.range ~= nil) then
		self.vars.maxx = self.vars.maxx or (self.x + self.range)
		self.vars.minx = self.vars.minx or (self.x - self.range)
	end

	-- Alright, first stab at this.  Check state.
	local state = self.action
	
	-- Walking
	if(state == 'WALK') then

		--[[if(self.refs.tattle ~= nil and self.vars.atkTimer <= 0) then
			self.refs.tattle = nil
			self.vars.atkTimer = love.math.random(1,4)
			return (self:fetchItemAction(1,'stand'))
		end]]--
		
		-- First check wall coll.  If we're hitting one, stand, then about face.
		local wlst = 'wallRt'
		if(self.facing == -1) then wlst = 'wallLt' end
		if(self[wlst] == 1) then
			self.vars.aiTimer = 1
			self.vars.nextDir = self.facing * -1
			return 'STAND.1'
		end
	
		-- Same check for range.
		if(self.range ~= nil) then
			if((self.facing == 1 and self.x >= self.vars.maxx) or 
				(self.facing == -1 and self.x <= self.vars.minx))
			then
				self.vars.aiTimer = 1
				self.vars.nextDir = self.facing * -1
				return 'STAND.1'
			end
		end
	
		-- Find floor tile under our front edge.
		local bnd = self:getBounds()
		local px = math.floor(bnd[1 + self.facing])
		local py = math.floor(bnd[3] + 1)
		local myCam = self.camera or -1
		
		local lTile = self.scene:getTile(myCam, px, py)

		-- If it's missing/empty, then we stop.
		if(lTile ~= nil) then
			local coll = tonumber(lTile.coll) or 0
			if(coll == 0) then
				self.vars.aiTimer = 1
				self.vars.nextDir = self.facing * -1
				return 'STAND.1'
			end
		else
			self.vars.aiTimer = 1
			self.vars.nextDir = self.facing * -1
			return 'STAND.1'
		end

	-- Standing?  Wait for timer, then turn around and go.		
	elseif (state == 'STAND') then

		--[[if(self.refs.tattle ~= nil and self.vars.atkTimer <= 0) then
			self.refs.tattle = nil
			self.vars.atkTimer = love.math.random(1,4)
			return (self:fetchItemAction(1,'stand'))
		end]]--
		
		if(self.vars.aiTimer <= 0) then
			self.facing = self.vars.nextDir or self.facing
			self.vars.nextDir = nil
			return 'WALK.1'
		end	
	end

end

function AI.goblinAttackSimple(self, dt)

	self.vars.walkadj = .8

	if(self.refs.tattle == nil) then return AI.goblinPatrol(self,dt) end
	
	local that = self.refs.tattle
	
	if(that.current.hp <= 0) then 
		self.refs.tattle = nil
		return AI.goblinPatrol(self,dt)
	end
	
	if((math.abs(self:getY() - that:getY()) > 2) or (math.abs(self:getX() - that:getX()) > 8)) then 
		self.vars.lastSeen = (self.vars.lastSeen or 10) - dt
		if(self.vars.lastSeen <= 0) then self.refs.tattle = nil end
		return AI.goblinPatrol(self,dt) 
	end
	
	self.vars.lastSeen = 10
	
	self.vars.aiTimer = (self.vars.aiTimer or 0) - (dt or 0)
	self.vars.atkTimer = (self.vars.atkTimer or 0) - (dt or 0)

	local dist = that:getX() - self:getX()
	
	local dir = 1
	if(dist < 0) then dir = -1 end
	
	local aDist = math.abs(dist)
	
	if(aDist < 1.25) then
		if(self.vars.atkTimer <= 0) then
			self.facing = dir
			self.vars.atkTimer = love.math.random(1,4)/2
			return (self:fetchItemAction(1,'stand'))
		else
			self.facing = -dir
			return AI.tryWalk(self) --'WALK.1' 
		end
	elseif(aDist > 2.25) then
		self.facing = dir
		return AI.tryWalk(self) --'WALK.1'
	else
		self.facing = dir
		if(self.vars.atkTimer <= 0) then
			self.vars.atkTimer = love.math.random(1,4)/2
			return (self:fetchItemAction(1,'stand'))
		else
			if(self.action ~= 'STAND') then return 'STAND.1' end
		end
	end

end

function AI.tryWalk(self)

	-- First check wall coll.  If we're hitting one, stand, then about face.
	local wlst = 'wallRt'
	if(self.facing == -1) then wlst = 'wallLt' end
	if(self[wlst] == 1) then
		self.vars.aiTimer = 1
		self.vars.nextDir = self.facing * -1
		return nil --'STAND.1'
	end

	-- Find floor tile under our front edge.
	local bnd = self:getBounds()
	local px = math.floor(bnd[1 + self.facing] + self.facing)
	local py = math.floor(bnd[3] + 1)
	local myCam = self.camera or -1
	
	local lTile = self.scene:getTile(myCam, px, py)

	-- If it's missing/empty, then we stop.
	if(lTile ~= nil) then
		local coll = tonumber(lTile.coll) or 0
		if(coll == 0) then
			self.vars.aiTimer = 1
			self.vars.nextDir = self.facing * -1
			return 'STAND.1'
		end
	else
		self.vars.aiTimer = 1
		self.vars.nextDir = self.facing * -1
		return 'STAND.1'
	end

	if(self.action ~= 'WALK') then return 'WALK.1' end
	
	return nil

end
