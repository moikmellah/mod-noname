--[[
Copyright 2016 MoikMellah

This file is part of the NoName sample module for use with the SYPHA Engine.

The NoName module is free software: you can redistribute it and/or modify it under the terms
of the CC-By-SA 4.0 license as published by Creative Commons.

This code is provided strictly as-in and WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the included file 'LICENSE.CC-BY-SA.txt' for full details.

You should have received a copy of the CC-By-SA 4.0 License along with
the NoName module. If not, see https://creativecommons.org/licenses/by-sa/4.0/
]]--

require('classes.Actor')
require('utils.Inheritance')

InvIcon = Inherit(nil, Actor)

function InvIcon:setIcon()

	if not(self.slot == 'skill' or contains(self.slot, 'nav')) then
		local hero = self.scene.hero or self.scene.env.vars.hero
		if(hero == nil) then return nil end
		local k = self.invPos or 0
		local i = hero.inv.item[k]
		if(i ~= nil) then
			self.skillName = i.skillId
		else
			self.skillName = nil
		end
	end

	if(self.skillName ~= nil or self.slot == 'skill') then
		return self:setSingleSkillIcon()
	elseif(contains(self.slot, 'nav')) then
		return self:setSingleNavIcon()
	else
		return self:setSingleInvIcon()
	end

end

function InvIcon:setSingleInvIcon()

--[[	if(self.skillName ~= nil) then
		return self:setSingleSkillIcon()
	end
]]--
	local hero = self.scene.hero or self.scene.env.vars.hero
	
	if(hero == nil) then return nil end

	local k = self.invPos or 0

	if(self.slot ~= nil) then
		self.prim[2].visible = 1
	end

	if(hero.inv.item[k] ~= nil) then
		local item = hero.inv.item[k].itemId
		local itemTmp = nil
		if(item ~= nil) then
			itemTmp = self.scene.env.itemLib.items[item].pickupTemplate
			self.prim[3].visible = 1
			self.prim[3].imageSet = itemTmp.imageSet
			self.prim[3].quad = 'ICON-2'
			self.prim[2].quad = 'empty'
			self.skillName = nil
			self.prim[5].visible = 0
		else
			skill = hero.inv.item[k].skillId
			skillTmp = self.scene.env.skillLib.skills[skill].pickupTemplate
			self.skillName = skill
			self.prim[3].visible = 1
			self.prim[3].imageSet = skillTmp.imageSet
			self.prim[3].quad = 'ICON-2'
			self.prim[2].quad = 'empty'
		end
	else
		self.skillName = nil
		self.prim[3].visible = 0
		self.prim[5].visible = 0
		if(self.slot ~= 'inv') then
			self.prim[2].quad = self.slot
		else
			self.prim[2].quad = 'empty'
		end
	end

end

function InvIcon:setSingleSkillIcon()

	local itemTmp = nil

	if(self.slot ~= nil) then
		self.prim[2].visible = 1
	end

	itemTmp = self.scene.env.skillLib.skills[self.skillName].pickupTemplate
	self.prim[3].visible = 1
	self.prim[3].imageSet = itemTmp.imageSet
	self.prim[3].quad = 'ICON-2'
	self.prim[2].quad = 'empty'

	local hero = self.scene.hero or self.scene.env.vars.hero
	
	if(hero == nil) then return nil end

	hero.base.skills = hero.base.skills or {}
	local heroSkills = hero.base.skills
	
	local skillRecord = heroSkills[self.skillName] or {}
	local skillLv = skillRecord.level or 0
	
	if(skillLv == 0) then
		self.prim[3].color = self.prim[3].color or {r=.7, g=.7, b=.7, a=.5, shader='colorFade'}
		--self.prim[5].visible = 1
	else
		self.prim[3].color = nil
		self.prim[5].visible = 1
		self.prim[5].text = 'L' .. skillLv
	end

end

function InvIcon:setSingleNavIcon()

	local itemTmp = nil

	if(self.slot ~= nil) then
		self.prim[2].visible = 1
		self.prim[2].quad = 'empty'
	end

end

function InvIcon:getContextMenu()

	if(self.slot == nil) then return nil end
	
	if(self.slot == 'skill') then
		return self:getSkillContextMenu()
	elseif(self.slot ~= nil) then
		return self:getItemContextMenu()
	end

	return nil

end

function InvIcon:getSkillContextMenu()

	local opts = {}

	local itemTmp = self.scene.env.skillLib.skills[self.skillName]

	local hero = self.scene.hero or self.scene.env.vars.hero
	
	if(hero == nil) then return nil end

	hero.base.skills = hero.base.skills or {}
	local heroSkills = hero.base.skills
	
	local skillRecord = heroSkills[self.skillName] or {}
	local skillLv = skillRecord.level or 0
	
	-- Equip
	if(itemTmp.skillType == 'action' and skillLv > 0) then
		table.insert(opts,'Equip:notifyListener:equipSkill,'..self.skillName)
	end
	
	-- Unlock/Upgrade
	if(skillLv == 0) then
		table.insert(opts,'Unlock ['..(itemTmp.cost or 1)..' SP]:notifyListener:upgradeSkill,'..self.skillName)
	elseif(skillLv < (itemTmp.maxLevel or 99)) then
		table.insert(opts,'Upgrade ['..(itemTmp.cost or 1)..' SP]:notifyListener:upgradeSkill,'..self.skillName)
	end

	if(#opts > 0) then
		table.insert(opts,'Cancel:exitMenu')
	end

	if(#opts > 0) then
		return table.concat(opts,'|')
	end

	return nil

end

function InvIcon:getItemContextMenu()

	local opts = {}

	local hero = self.scene.hero or self.scene.env.vars.hero
	
	if(hero == nil) then return nil end

	hero.inv = hero.inv or {}
	hero.inv.item = hero.inv.item or {}
	
	local item = hero.inv.item[self.invPos]
	local itemTmp = nil
	if(item ~= nil) then
		itemTmp = self.scene.env.itemLib.items[item.itemId] or {}
	else
		return nil
	end
	
	-- Equip
	if(itemTmp.eq ~= nil and self.invPos > 13) then
		table.insert(opts,'Equip/Move:notifyListener:equipItem,'..self.invPos)
	end
	
	-- Unequip
	if(self.invPos <= 13) then
		table.insert(opts,'Unequip/Move:notifyListener:unequipItem,'..self.invPos)
--	elseif(self.invPos <= 13 and self.skillName ~= nil) then
--		table.insert(opts,'Unequip:notifyListener:unequipSkill,'..self.invPos)
	end
	
	-- Discard
	if(self.skillName == nil) then
		table.insert(opts,'Drop:notifyListener:discardItem,'..self.invPos)
	end

	if(#opts > 0) then
		table.insert(opts,'Cancel:exitMenu')
	end

	if(#opts > 0) then
		return table.concat(opts,'|')
	end

	return nil

end
