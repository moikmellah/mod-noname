--[[
Copyright 2016 MoikMellah

This file is part of the NoName sample module for use with the SYPHA Engine.

The NoName module is free software: you can redistribute it and/or modify it under the terms
of the CC-By-SA 4.0 license as published by Creative Commons.

This code is provided strictly as-in and WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the included file 'LICENSE.CC-BY-SA.txt' for full details.

You should have received a copy of the CC-By-SA 4.0 License along with
the NoName module. If not, see https://creativecommons.org/licenses/by-sa/4.0/
]]--

function Actor:fetchItemAction(i_item, position)

	local it = i_item or 1
	local pos = position or 'stand'

	local itemId = 'PUNCH'

	self.inv = self.inv or {}
	self.inv.item = self.inv.item or {}

	local myItem = self.inv.item[it]

	if(type(myItem) == 'table') then
		-- Spell stuff.
		if(myItem.skillId ~= nil) then 
			return self:fetchSkillAction(myItem.skillId, pos)
		end
		itemId = myItem.itemId or 'PUNCH'
	end

	local item = self.scene.env.itemLib.items[itemId]
	
	if(type(item) == 'table') then
		if(item.class == 'melee') then
			if(string.find(item.attrs, 'wpnheavy') ~= nil) then
				self.vars.atkWeightMult = gconf.heavyWpnSpeed or 2
			elseif(string.find(item.attrs, 'wpnmedium') ~= nil) then
				self.vars.atkWeightMult = gconf.mediumWpnSpeed or 1.5
			else
				self.vars.atkWeightMult = nil
			end
		else
			self.vars.atkWeightMult = nil
		end
	end

	self.vars.nxHelper = self:assembleItemTemplate(i_item, true)
	
	return item.atkAction[pos]

end

function Actor:fetchSkillAction(skillId, pos)

	-- Make sure it exists
	local skill = self.scene.env.skillLib.skills[skillId]
	if(skill == nil) then return nil end

	-- Make sure skill is applicable
	local act = skill.atkAction[pos]
	if(act == nil) then return nil end

	-- Assess mana cost
	local manaCost = tonumber(skill.manaCost) or 0
	if((self.current.mp or 0) < manaCost) then return nil end

	self.current.mp = (self.current.mp or 0) - manaCost
	self.vars.nxSkill = skillId
	return act

end

function Actor:handleSkill()

	local skillId = self.vars.nxSkill
	self.vars.nxSkill = nil
	if(skillId == nil) then return nil end
	
	local skill = self.scene.env.skillLib.skills[skillId]
	if(skill == nil) then return nil end
	
	local fct = skill.initscript
	if(type(fct) ~= 'function') then
		fct = loadstring('local self,that = ... '.. skill.initscript)
		skill.initscript = fct
	end
	
	fct(self,skill)
	
end

function Actor:spawnSpell(that)

	local fb = self:spawn(that.helperTemplate, self:getHandX(), self:getHandY() + units(6), self.z)
	fb.facing = self.facing
	fb.faction = self.faction
	
	fb.vars.dmg = self:getSkillDamage(that)
	fb.refs = fb.refs or {}
	fb.refs.parentObj = self
	fb.xv = fb.xv * self.facing

end

function Actor:getSkillDamage(skill)

	local fb = skill.helperTemplate
	local dmgMult = fb.vars.dmgMult or 1
	
	local heroSkills = self.base.skills or {}
	local skillRecord = heroSkills[skill.name] or {}
	local skillLv = skillRecord.level or 1
	local lvMult = (fb.vars.lvMult or 0) * (skillLv - 1)
	
	local retdmg = self.current.int * ((fb.vars.dmgMult or 1) + lvMult) * self:getDamageMultiplier(fb.attrs)

	return retdmg

end

function Actor:getDamageMultiplier(attrs)

	local t_attrs = strToTable(attrs or '', ',')
			
	local atkmult = 1
	
	local attrdone = {}
	
	for ak,av in ipairs(t_attrs) do
		if(attrdone[av] == nil) then 
			atkmult = atkmult + (self.current['dmg' .. av] or 0)
			
			local ca = string.gsub(av, '^wpn', '')
			ca = string.gsub(ca, '^def', '')
			
			attrdone[av] = true 
		end
	end
	
	return atkmult

end

function Actor:getSkillDescr(skillId)

	local skill = self.scene.env.skillLib.skills[skillId]

	local heroSkills = self.base.skills or {}
	local skillRecord = heroSkills[skill.name] or {}
	local level = skillRecord.level or 0
	local skillLv = skillRecord.level or 1
	
--[[	local fb = skill.helperTemplate
	local dmgMult = fb.vars.dmgMult or 1
	local lvMult = (fb.vars.lvMult or 0) * (skillLv - 1)
	
	local retdmg = self.current.int * ((fb.vars.dmgMult or 1) + lvMult) * self:getDamageMultiplier(fb.attrs)
]]--
	local retStr = ''
	if(level > 0) then retStr = retStr .. 'Lv '..level..' ' end
	retStr = retStr .. skill.displayName .. '\n'
	
	local d = skill.descr or 'Improves abilities.'
	local fst = 1	-- circuit breaker!
	local a,b,m = string.find(d,'%[(%d+)%]')
	while(a ~= nil and fst < 10) do
		local nm = (tonumber(m) or 0) * skillLv
		d = string.gsub(d,'%[%d+%]',nm,1)
		a,b,m = string.find(d,'%[(%d+)%]')
		fst = fst + 1
	end

	retStr = retStr .. d

	return retStr

end

function Actor:assembleDropTemplate(s_item, b_randomizeMods, i_lvl)

	if(s_item == nil) then return nil end
	
	local item = self.scene.env.itemLib.items[s_item]
	
	if(item == nil) then return nil end
	
	local retItem = tableCopy(item.pickupTemplate)
	
	retItem.itemId = s_item
	
	if(b_randomizeMods) then

		retItem.mod = self:getRandomMods(item.class,i_lvl)

	end
	
	return retItem

end

function Actor:getRandomMods(class,lvl)

	local retMods = {}

	for k = 1,3 do

		if(love.math.random() < (gconf.itemModChance or .3)) then
		
			retMods[k] = self.scene.env.modLib:getRandomMod(class, k, lvl)
		
		end

	end
	
	return retMods

end

function Actor:pickupItem(that)

	if(that == nil) then return nil end

	self.inv = self.inv or {}
	
	self.inv.item = self.inv.item or {}
	
	local p = nil
	local invMax = gconf.invMax or 61
	
	for k = 14,invMax do
		if(self.inv.item[k] == nil) then
			p = k
			break
		end
	end
	
	if(p ~= nil) then
		self.inv.item[p] = { itemId = that.itemId, level = that.vars.level }
		if(that.mod ~= nil) then
			self.inv.item[p].mod = tableCopy(that.mod)
		end
		self.scene:remove(that)
		
		local tItem = self:assembleItemTemplate(p)
		
		local lbl = self:spawn('introLabel', 16, 15, 10)
		lbl.camera = 2
		lbl.prim[1].text = tItem.displayName
		lbl.prim[1].bkgColor.r = .8 
		lbl.prim[1].bkgColor.b = 0

	end

end

function Actor:assembleItemTemplate(i_item, withDefault)

	local it = i_item or 1

	local itemId = 'PUNCH'

	self.inv = self.inv or {}
	self.inv.item = self.inv.item or {}

	local myItem = self.inv.item[it]

	local itemLvl = 1

	if(type(myItem) == 'table') then
		itemId = myItem.itemId or 'PUNCH'
		itemLvl = myItem.level or 1
	elseif withDefault == true then
		itemId = 'PUNCH'
	else
		return nil
	end
	
	local statBase = Actor:getStatMultiplier(itemLvl)

	local item = self.scene.env.itemLib.items[itemId]
	local prefix = ''
	local suffix = ''
	local cleanAttrs = 'Attrs: '

	local nx = tableCopy(item.helperTemplate)

	nx.id = item.id
	nx.attrs = item.attrs
	nx.attack = tableCopy(item.attack) or {}
	nx.equip = tableCopy(item.equip) or {}

	nx.stats = {}

	nx.shortName = item.displayName or 'Thing'

	--nx.displayName = item.displayName or 'Thing'
	
	nx.descr = item.descr

	if(type(myItem) == 'table' and type(myItem.mod) == 'table') then
		for k = 1,3 do
			local mid = myItem.mod[k]
			local m = self.scene.env.modLib.mods[mid]
			
			if(type(m) == 'table') then
			
				if(m.descr ~= nil) then nx.descr = nx.descr .. ' ' .. m.descr end
			
				if(k == 3) then
					suffix = ' ' .. m.title
				else
					prefix = prefix .. m.title .. ' '
				end
			
				nx.attack = nx.attack or {}
				nx.attack.dmg = (nx.attack.dmg or 0) * (1 + (m.mult or 0))
				nx.equip.def = (nx.equip.def or 0) * (1 + (m.mult or 0))
				nx.equip.mdef = (nx.equip.mdef or 0) * (1 + (m.mult or 0))
				
				if(type(m.attrs) == 'string') then nx.attrs = (nx.attrs or '') .. ',' .. m.attrs end
				
				if(type(m.attack) == 'table') then
					if(type(m.attack.buff) == 'table') then
						nx.attack.buff = nx.attack.buff or {}
						for mbk,mbv in pairs(m.attack.buff) do
							if(type(mbv) == 'table') then 
								local mbf = tableCopy(mbv)
								mbf.name = m.id .. '-' .. mbk
								table.insert(nx.attack.buff, mbf)
							end
						end
						--nx.attack.buff = tableCopy(m.attack.buff)
					end
					for k,v in pairs(m.attack) do
						if(type(v) == 'number') then
							nx.attack[k] = (nx.attack[k] or 0) + v
						end
					end
				end
				if(type(m.equip) == 'table' and type(m.equip.buff) == 'table') then
					for bk,bv in pairs(m.equip.buff) do
						for ek,ev in pairs(bv) do
							if(type(ev) == 'number') then
								local lvmult = statBase
								if(bv.flat == 1) then lvmult = 1 end
								nx.stats[ek] = math.ceil((nx.stats[ek] or 0) + (ev * lvmult))
							end
						end
					end
				end
			end
			
		end
	
	end
	
	local t_attrs = strToTable(nx.attrs or '', ',')
			
	local atkmult = 1
	local defmult = 1
	
	local attrdone = {}
	
	local amore = false
	
	for ak,av in ipairs(t_attrs) do
		if(attrdone[av] == nil) then 
			atkmult = atkmult + (self.current['dmg' .. av] or 0)
			defmult = defmult + (self.current['def' .. av] or 0)
			
			local ca = string.gsub(av, '^wpn', '')
			ca = string.gsub(ca, '^def', '')
			
			if(amore) then
				cleanAttrs = cleanAttrs .. ','
			else
				amore = true
			end
			
			cleanAttrs = cleanAttrs .. string.upper(ca)
						
			attrdone[av] = true 
		end
	end
	
	nx.attack.dmg = (nx.attack.dmg or 0) * atkmult * statBase
	nx.equip.def = (nx.equip.def or 0) * defmult * statBase
	nx.equip.mdef = (nx.equip.mdef or 0) * defmult * statBase

	nx.stats.id = nil
	if(nx.attack.dmg > 0) then nx.stats.atk = math.ceil(nx.attack.dmg) end
	if(nx.equip.def > 0) then nx.stats.def = math.ceil(nx.equip.def) end
	if(nx.equip.mdef > 0) then nx.stats.mdef = math.ceil(nx.equip.mdef) end
	
	local more = false
	local statStr = ''
	
	for kk,kv in pairs(nx.stats) do
		if more then
			statStr = statStr .. ', '
		else
			more = true
		end
		
		statStr = statStr .. string.upper(kk) .. ' '
		if(type(kv) == 'number' and kv >= 0) then
			statStr = statStr .. '+'
		end
		statStr = statStr .. kv
		
	end
	
	nx.displayName = 'Lv ' .. itemLvl .. ' ' .. prefix .. nx.shortName .. suffix
	
	nx.fulltext = nx.displayName
	nx.fulltext = nx.fulltext .. '\n' .. (nx.descr or '')
	nx.fulltext = nx.fulltext .. '\n' .. (statStr or '')
	nx.fulltext = nx.fulltext .. '\n' .. (cleanAttrs or '')
	
--	--debugPrint(nx)
	
	return nx

end

function Actor:tossItem()

	-- Find first throwable
	local itemPos = self:getTossItemPos()
	if(itemPos == nil) then return nil end
	
	-- Spawn item template
	local itemTmp = self:assembleItemTemplate(itemPos)
	if(itemTmp == nil) then return nil end
	
	itemTmp.objType = 'projectile'
	itemTmp.actionSet = 'thrownItem'
	itemTmp.action = 'thrownItem'
	itemTmp.frame = 1
	local item = self:spawn(itemTmp, self:getHandX(), self:getHandY(), self.z)
	
	-- Remove from inventory
	self.inv.item[itemPos] = nil
	
	-- Update it
	item.xv = 10 * self.facing
	item.facing = self.facing
	item.yv = -8
	item.attack = item.attack or {}
	item.attack.dmg = (item.attack.dmg or 1) --* 2
	item.doinertia = 1
	item.faction = self.faction
	item.refs = item.refs or {}
	item.refs.parentObj = self
	item.position = nil

end

function Actor:getTossItemPos()

	self.inv = self.inv or {}
	self.inv.item = self.inv.item or {}

	-- Find first throwable
	for i = 10,13 do
		if(self.inv.item[i] ~= nil) then return i end
	end

	return nil

end

function Actor:getRandomItem(class, opener, maxLv)

	local ml = maxLv or 1
	
	local ic = class or 'melee'
	
	local o = opener or self.scene.hero or self.scene.env.vars.hero
	
	local item = Queryable.new(self.scene.env.itemLib.items
		):Where('x => (x.pickupTemplate ~= nil) and (x.class == "'..ic..'") and ((x.minLevel or 999) <= '..ml..') and ((x.maxLevel or 999) >= '.. ml ..')'
		):Select('x => {id = x.id, sort = love.math.random()}'
		):OrderBy('x => x.sort'
		):First()
		
	return item.id

end

function Actor:spawnDropItems()

	self.vars.dropList = self.vars.dropList or {}
	local dropList = self.vars.dropList
	
	if(#dropList == 0) then return nil end
	
	for i = 1,#dropList do
	
		local oldItem = dropList[i]
		local dropItem = self:assembleDropTemplate(oldItem.itemId)
		dropItem.mod = oldItem.mod
		dropItem.vars = dropItem.vars or {}
		dropItem.vars.level = oldItem.level
		--dropItem.vars.boundColor = { r='1', g='.2', b='.2', a='.4' }
		
		local p = self:spawn(dropItem, self.x, self.y, self.z)
		p.xv = (i-math.ceil(#dropList/2)) * 3
		p.yv = -8
	
	end
	
	self.vars.dropList = {}

end
