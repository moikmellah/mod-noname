--[[
Copyright 2016 MoikMellah

This file is part of the NoName sample module for use with the SYPHA Engine.

The NoName module is free software: you can redistribute it and/or modify it under the terms
of the CC-By-SA 4.0 license as published by Creative Commons.

This code is provided strictly as-in and WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the included file 'LICENSE.CC-BY-SA.txt' for full details.

You should have received a copy of the CC-By-SA 4.0 License along with
the NoName module. If not, see https://creativecommons.org/licenses/by-sa/4.0/
]]--

require('classes.Actor')
require('utils.Inheritance')

InvCursor = Inherit(nil, Actor)

function InvCursor:equipmentUiNav(s_btn)

	local btn = s_btn or 'right'
	
	local nbtn = string.sub(btn,1,1)

	self.vars.posId = self.vars.posId or 1

	local oldId = self.vars.posId

--[[
	local op = self.vars.pos[self.vars.posId] or {}
	self.vars.posId = op[nbtn] or self.vars.posId
	local np = self.vars.pos[self.vars.posId]
	self.prim[1].x = np.x
	self.prim[1].y = np.y
	self.prim[1].w = np.w or 1
	self.prim[1].h = np.h or 1
]]--

	self.vars.guiArray = self.vars.guiArray or {}

	local op = self.vars.guiArray[self.vars.posId]

	if(op == nil) then return nil end
	
	self.vars.posId = op[nbtn] or self.vars.posId

	local np = self.vars.guiArray[self.vars.posId]
	
	op.prim[4].visible = 0
	np.prim[4].visible = 1

	local hero = self.refs.sourceActor
	
	if(np.vars.descr ~= nil) then
		self.prim[9].text = np.vars.descr
	elseif(np.skillName ~= nil) then
		self.prim[9].text = hero:getSkillDescr(np.skillName)
	elseif(hero ~= nil) then
		local ninv = hero.inv.item[self.vars.posId]
		if(ninv ~= nil and ninv.skillId ~= nil) then
			self.prim[9].text = hero:getSkillDescr(ninv.skillId)
		else
			local nx = hero:assembleItemTemplate(self.vars.posId)
			if(nx ~= nil) then
				self.prim[9].text = nx.fulltext
			else
				self.prim[9].text = 'Empty'
			end
		end
	end

end

function InvCursor:handleClick()
	
	local op = self.vars.guiArray[self.vars.posId]
		
	if(op == nil) then return nil end
	
	if(self.vars.selected ~= nil) then
		self:equipmentClick()
	elseif(op.slot == 'skill') then
		--self:equipmentClick()
		local menu = op:getContextMenu()
		if(menu == nil) then return nil end
		
		local subMenu = self:spawn('menuOption', op.x + 1, op.y, op.z + 10)
		subMenu.menuOptions = menu
		subMenu.refs.listener = self
		subMenu.refs.parentMenu = self
		subMenu:initMenu()
		self.refs.menuObj = subMenu
		return 'listen.1'
	elseif(contains(op.slot, 'nav-')) then
		local a,b,c = string.find(op.slot,'nav%-(%d)')
		local which = tonumber(c) or 0
		if(which > 0) then
			self:setupSkillGui(which)
		else
			self:setupInventoryGui()
		end
	else
		--self:equipmentClick()
		local menu = op:getContextMenu()
		if(menu == nil) then return nil end
		
		local subMenu = self:spawn('menuOption', op.x + 1, op.y, op.z + 10)
		subMenu.menuOptions = menu
		subMenu.refs.listener = self
		subMenu.refs.parentMenu = self
		subMenu:initMenu()
		self.refs.menuObj = subMenu
		return 'listen.1'
	end
	
end

function InvCursor:equipmentClick()

	if(self.vars.selected == nil) then
		
		local op = self.vars.guiArray[self.vars.posId]
		
		if(op == nil) then return nil end

		self.vars.selected = self.vars.posId
		
		op.prim[1].visible = 1
		
	else
	
		local hero = self.refs.sourceActor
		local i = hero.inv.item
	
		local opa = self.vars.guiArray[self.vars.selected]
		local opb = self.vars.guiArray[self.vars.posId]

		if(opa.slot == 'skill' and opb.slot == 'act') then
			local p = nil
			local invMax = gconf.invMax or 61
			for k = 14,invMax do
				if(i[k] == nil) then
					p = k
					break
				end
			end
			if(p ~= nil) then
				hero.inv.item[p] = hero.inv.item[opb.invPos]
			else
				self:discardItem(opb.invPos)
			end
			hero.inv.item[opb.invPos] = {
				skillId = opa.skillName,
				level = 1
			}
		elseif(self:canSwapItems(self.vars.posId, self.vars.selected)) then
			if(opa.slot ~= opb.slot) then
				if(opa.skillName) then i[self.vars.selected] = nil end
				if(opb.skillName) then i[self.vars.posId] = nil end
			end
			i[self.vars.posId],i[self.vars.selected] = i[self.vars.selected],i[self.vars.posId]
		end
		
		self.vars.guiArray[self.vars.selected].prim[1].visible = 0
		
		self.vars.selected = nil

		hero:setSkillBuffs()
		hero:setEquipmentBuffs()
		hero:rollStats()

		self:getHeroStats()		
		self:equipmentUiNav('none')
		
	end

end



function InvCursor:canSwapItems(fromPos, toPos)

	local posA = fromPos or 1
	local posB = toPos or 1
	
	local hero = self.refs.sourceActor
	hero.inv = hero.inv or {}
	hero.inv.item = hero.inv.item or {}
	
	local itemIdA = 'none'
	local itemTmpA = {}
	local itemIdB = 'none'
	local itemTmpB = {}
	
	if(hero.inv.item[posA] ~= nil) then
		itemIdA = hero.inv.item[posA].itemId
		if(itemIdA ~= nil) then
			itemTmpA = self.scene.env.itemLib.items[itemIdA]
		else
			itemIdA = hero.inv.item[posA].skillId
			itemTmpA = self.scene.env.skillLib.skills[itemIdA]
		end
	end
	if(hero.inv.item[posB] ~= nil) then
		itemIdB = hero.inv.item[posB].itemId
		if(itemIdB ~= nil) then
			itemTmpB = self.scene.env.itemLib.items[itemIdB]
		else
			itemIdB = hero.inv.item[posB].skillId
			itemTmpB = self.scene.env.skillLib.skills[itemIdB]
		end
	end
	
	local slotA = self.vars.guiArray[posA]
	local slotB = self.vars.guiArray[posB]
	
	if((itemIdA ~= 'none') and (string.find(itemTmpA.eq, slotB.slot) == nil) and (slotB.slot ~= 'inv')) then
		return false
	end
	if((itemIdB ~= 'none') and (string.find(itemTmpB.eq, slotA.slot) == nil) and (slotA.slot ~= 'inv')) then
		return false
	end
	
	return true

end

function InvCursor:getHeroStats()

	local hero = self.refs.sourceActor
	
	if(hero == nil) then return nil end

	local str = "\n\n"
	--str = str + "HP\nMP\nSTR\nCON\nINT\nWIS\nAGI\nLCK"
	str = str .. math.ceil(hero.current.hp) .. '/' .. math.ceil(hero.current.maxhp) .. '\n'
	str = str .. math.ceil(hero.current.mp) .. '/' .. math.ceil(hero.current.maxmp) .. '\n\n'
	str = str .. math.ceil(hero.current.def) .. '\n'
	str = str .. math.ceil(hero.current.mdef) .. '\n\n'
	str = str .. math.ceil(hero.current.str) .. '\n'
	str = str .. math.ceil(hero.current.con) .. '\n'
	str = str .. math.ceil(hero.current.int) .. '\n'
	str = str .. math.ceil(hero.current.wis) .. '\n'
	str = str .. math.ceil(hero.current.agi) .. '\n'
	str = str .. math.ceil(hero.current.lck) .. '\n\n'
	
	str = str .. hero.current.level .. '\n'
	str = str .. hero.base.xp .. '\n'
	str = str .. ((gconf.expPerLvl or 3000) * hero.current.level) .. '\n\n'

	self.prim[8].text = str

end


function InvCursor:linkGuiElements()

	local guiQuery = Queryable.new(self.vars.guiArray)
	
	for k,v in ipairs(self.vars.guiArray) do
		local x = v.x
		local y = v.y
		
		local downElement = guiQuery:Where('x => x.y > '..y):OrderBy('x => dist(x.x, x.y, '..x..', '..y..')'):First()
		if(downElement) then v.d = downElement.invPos end

		local upElement = guiQuery:Where('x => x.y < '..y):OrderBy('x => dist(x.x, x.y, '..x..', '..y..')'):First()
		if(upElement) then v.u = upElement.invPos end
		
		local leftElement = guiQuery:Where('x => x.x < '..x):OrderBy('x => dist(x.x, x.y, '..x..', '..y..')'):First()
		if(leftElement) then v.l = leftElement.invPos end

		local rightElement = guiQuery:Where('x => x.x > '..x):OrderBy('x => dist(x.x, x.y, '..x..', '..y..')'):First()
		if(rightElement) then v.r = rightElement.invPos end
	end
end

function InvCursor:setupInventoryGui()
	self:dismissAllIcons()
	local invMax = gconf.invMax or 61

	self.refs.sourceActor = self.scene.env.vars.hero
	self.vars.guiArray = {}
	--[[
	for i = 1,64 do
		local j = i-1
		local thing = self:spawn('invIcon', 2+(1.5*math.floor(j/4)), 2+(1.5*math.floor(j%4)), 0)
		thing.invPos = i
		self.vars.guiArray[i] = thing
	end
	]]--
	
	-- Action slots
	for i = 1,3 do
		local j = i-1
		local thing = self:spawn('invIcon', 5+(1.5*j), 3.5, 0)
		thing.invPos = i
		thing.slot = 'act'
		self.vars.guiArray[i] = thing
	end
	
	-- Equipment slots
	for i = 4,9 do
		local j = i-4
		local thing = self:spawn('invIcon', 15+(1.5*j), 3.5, 0)
		thing.invPos = i
		self.vars.guiArray[i] = thing
	end
	self.vars.guiArray[4].slot = 'shield'
	self.vars.guiArray[5].slot = 'armor'
	self.vars.guiArray[6].slot = 'helm'
	self.vars.guiArray[7].slot = 'boot'
	self.vars.guiArray[8].slot = 'accessory'
	self.vars.guiArray[9].slot = 'accessory'
	
	-- Ammo slots
	for i = 10,13 do
		local j = i-10
		local thing = self:spawn('invIcon', 5, 7.5+(1.5*j), 0)
		thing.invPos = i
		thing.slot = 'ammo'
		self.vars.guiArray[i] = thing
	end
	
	-- Inventory slots
	for i = 14,invMax do
		local j = i-14
		local ix = 8+(1.5*math.floor(j/4))
		local iy = 7.5+(1.5*math.floor(j%4))
		local thing = self:spawn('invIcon', ix,iy, 0)
		thing.invPos = i
		thing.slot = 'inv'
		self.vars.guiArray[i] = thing
	end
	
	local skillLib = self.scene.env.skillLib
	
	local classList = self.refs.sourceActor.base.classList or {}
	
	-- Navigation
	for i = 1,(#classList + 1) do
		local j = i-1
		local thing = self:spawn('invIcon', 2, 7.5+(1.5*j), 0)
		thing.slot = 'nav-'..j
		if(i > 1) then
			thing.vars.descr = 'Skills: ' .. skillLib:getClassName(classList[j])
		else
			thing.vars.descr = 'Inventory'
		end
		table.insert(self.vars.guiArray,thing)
		thing.invPos = #self.vars.guiArray
		if(i == 1) then self.vars.posId = #self.vars.guiArray end
	end
	

	
	-- Cursor obj
	--local curs =  self.vars.curs or self:spawn('invCursor',0,0)
	self:linkGuiElements()
	for i = 4,6 do
		self.prim[i].visible = 1
	end
	self.prim[10].visible = 0
	
	--Cleanup
	for i = 1,#self.vars.guiArray do
		self.vars.guiArray[i]:setIcon()
	end
	
	--self.vars.posId = 1
	self.vars.selected = nil
	self.vars.myClass = nil
	self:getHeroStats()
	self:equipmentUiNav('none')
end

function InvCursor:setupSkillGui(classId)
	self:dismissAllIcons()
	self.refs.sourceActor = self.scene.env.vars.hero
	
	-- Action slots
	for i = 1,3 do
		local j = i-1
		local thing = self:spawn('invIcon', 5+(1.5*j), 3.5, 0)
		thing.slot = 'act'
		table.insert(self.vars.guiArray,thing)
		thing.invPos = #self.vars.guiArray
	end

	local myClass = self.refs.sourceActor.base.classList[classId] or 'warrior'

	local skillLib = self.scene.env.skillLib
	local skillArray = skillLib.skills
	
	local skills = Queryable.new(skillArray):Where('x => contains(x.class,"'..myClass..'") ~= nil'
				):OrderBy('x => x.displayName'
				):ToArray()
	
	-- Skill slots
	for i = 1,#skills do
		local j = i-1
		local ix = 5+(3*math.floor(j/2))
		local iy = 7.5+(3*math.floor(j%2))
		local thing = self:spawn('invIcon', ix,iy, 0)
		thing.skillName = skills[i].name
		thing.slot = 'skill'
		table.insert(self.vars.guiArray,thing)
		thing.invPos = #self.vars.guiArray
	end
	
	local classList = self.refs.sourceActor.base.classList or {}
	
	-- Navigation
	for i = 1,(#classList + 1) do
		local j = i-1
		local thing = self:spawn('invIcon', 2, 7.5+(1.5*j), 0)
		thing.slot = 'nav-'..j
		if(i > 1) then
			thing.vars.descr = 'Skills: ' .. skillLib:getClassName(classList[j])
		else
			thing.vars.descr = 'Inventory'
		end
		table.insert(self.vars.guiArray,thing)
		thing.invPos = #self.vars.guiArray
		if(j == classId) then self.vars.posId = #self.vars.guiArray end
	end
	
	-- Other elements
	
	-- Cursor obj
	--local curs =  self.vars.curs or self:spawn('invCursor',0,0)
	self:linkGuiElements()
	for i = 4,6 do
		self.prim[i].visible = 0
	end
	self.prim[10].visible = 1
	self.prim[10].text = 'Skills: '..skillLib:getClassName(myClass)..' ('..self.refs.sourceActor:availableSp()..' SP)'
	
	--Cleanup
	for i = 1,#self.vars.guiArray do
		self.vars.guiArray[i]:setIcon()
	end
	
--	self.vars.posId = 1
	self.vars.selected = nil
	self.vars.myClass = myClass
	self:getHeroStats()
	self:equipmentUiNav('none')

end

function InvCursor:dismissAllIcons()
	self.vars.guiArray = self.vars.guiArray or {}
	
	for k,v in pairs(self.vars.guiArray) do
		self.scene:remove(v)
	end
	
	self.vars.guiArray = {}
end

function InvCursor:handleMenuInput(that)

	local verb,noun = strSplit(that.obj,',')
	
	-- Logic here
	if(verb == 'equipItem') then
		self:equipmentClick()
	elseif(verb == 'equipSkill') then
		self:equipmentClick()
	elseif(verb == 'upgradeSkill') then
		return self:upgradeSkill(noun)
	elseif(verb == 'discardItem') then
		self:discardItem(noun)
	else
		-- Default
		self:equipmentClick()
	end
	
	if(self.refs.menuObj) then
		-- Dismiss it
		self.scene:remove(self.refs.menuObj)
		return 'menu.1'
	end
	
	-- Back to normal operation
	return 'menu.1'
end

function InvCursor:discardItem(which)

	local invPos = tonumber(which)
	
	local hero = self.refs.sourceActor
	
	if(hero == nil) then return nil end
	
	local item = hero.inv.item[invPos]
	
	if(item ~= nil) then
		hero.vars.dropList = hero.vars.dropList or {}
		table.insert(hero.vars.dropList, item)
		hero.inv.item[invPos] = nil
		
		hero:setSkillBuffs()
		hero:setEquipmentBuffs()
		hero:rollStats()
		
		self:getHeroStats()
		self:equipmentUiNav('none')
	end

end

function InvCursor:upgradeSkill(skillId)

	local hero = self.refs.sourceActor
	
	hero.base.skills = hero.base.skills or {}
	local skill = hero.base.skills[skillId] or {id = skillId}

	local skillDef = self.scene.env.skillLib.skills[skillId]
	local cost = skillDef.cost or 1
	local avail = hero:availableSp() or 0
	
	if(avail < cost) then 
		-- play error sfx
		return nil 
	end
	
	skill.level = (skill.level or 0) + 1

	hero.base.skills[skillId] = skill

	self:resetCursor()

	if(self.refs.menuObj) then
		-- Dismiss it
		self.scene:remove(self.refs.menuObj)
	end

	return 'menu.1'
	
end

function InvCursor:resetCursor()

	local hero = self.refs.sourceActor
	local skillLib = self.scene.env.skillLib

	hero:setSkillBuffs()
	hero:setEquipmentBuffs()
	hero:rollStats()

	if(self.vars.myClass) then
		self.prim[10].text = 'Skills: '..skillLib:getClassName(self.vars.myClass)..' ('..hero:availableSp()..' SP)'
	end

	self:getHeroStats()
	self:equipmentUiNav('none')

end
