--[[
Copyright 2016 MoikMellah

This file is part of the NoName sample module for use with the SYPHA Engine.

The NoName module is free software: you can redistribute it and/or modify it under the terms
of the CC-By-SA 4.0 license as published by Creative Commons.

This code is provided strictly as-in and WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the included file 'LICENSE.CC-BY-SA.txt' for full details.

You should have received a copy of the CC-By-SA 4.0 License along with
the NoName module. If not, see https://creativecommons.org/licenses/by-sa/4.0/
]]--

require('utils.xml')

-- Table declaration for class
ModifierLibrary = {}

-- Metatable for class (to make ModifierLibrary table the default lookup for class methods)
ModifierLibrary_mt = {}
ModifierLibrary_mt.__index = ModifierLibrary

-- Constructor
function ModifierLibrary.new()

	-- Make copy from template if provided, or default template if not
	local nActLib = {}

	-- Set metatable to get our class methods
	setmetatable(nActLib, ModifierLibrary_mt)

	nActLib.mods = {}
	nActLib.count = 0

	return nActLib

end

function ModifierLibrary:init()

	if self.mods == nil then self.mods = {} self.count = 0 end
	self.list = self.list or {}

end

function ModifierLibrary:loadSet(s_fileName)

	--if(s_handle == nil) then return false end

	self:init()

	if(s_fileName == nil) then return false end

	if self.mods == nil then self.mods = {} end

	local aS = Xml.deserializeFile(s_fileName)

	for k,v in pairs(aS.mod) do
		self.mods[k] = v
	end

end

--[[

	Mod lists should be organized:
	- ItemClass
	-- Type
	--- Tier
	---- Mods
	
	Mods may be in multiple places.  Store ID only, not references.

]]--

function ModifierLibrary:buildLists()

	self.list = {}

	for k,v in pairs(self.mods) do
	
		local tier = v.tier or 1
		local iType = v.type or 2
		local items = v.items or 'melee'
		local iTab = strToTable(items, ',')
		
		-- Add to each item class list
		for ik,iv in pairs(iTab) do
			self.list[iv] = self.list[iv] or {}
			self.list[iv][iType] = self.list[iv][iType] or {}
			self.list[iv][iType][tier] = self.list[iv][iType][tier] or {}
			table.insert(self.list[iv][iType][tier], v.id)
		end
	
	end

	return nil	

end

function ModifierLibrary:getRandomMod(s_itemClass, i_type, i_maxTier)

	return self:getRandomModBeta(s_itemClass, i_type, i_maxTier)

end

function ModifierLibrary:getRandomModSimple(s_itemClass, i_type, i_minTier, i_maxTier)

	local class = s_itemClass or 'melee'
	local iType = i_type or 2

	local defMin = 1
	local defMax = 1

	local cList = self.list[class][iType]
	
	for k,v in pairs(cList) do
		if(k < defMin) then defMin = k end
		if(k > defMax) then defMax = k end
	end

	local minTier = i_minTier or defMin
	local maxTier = i_maxTier or defMax
	
	local tier = love.math.random(minTier, maxTier)
	
	local tList = cList[tier]
	
	if(tList == nil) then return nil end
	
	local index = love.math.random(1,#tList)
	
	return tList[index]

end

function ModifierLibrary:getRandomModBeta(s_itemClass, i_type, i_level)

	-- Initial query
	local q = Queryable.new(self.mods)
	
	-- Filter by type, and leave out reserved/disabled mods
	q = q:Where('x => x.type == ' .. i_type .. ' and x.minLevel ~= nil')

	-- Then by item class
	q = q:Where('x => string.match(x.items, "'.. s_itemClass ..'") ~= nil')

	-- Finally by level
	q = q:Where('x => (x.minLevel or 0) <= '.. (i_level or 0))
	
	q = q:Where('x => (x.maxLevel or 100) >= '.. (i_level or 100))

	-- Whatever's left, transform to a randomized list of id's
	q = q:Select('x => {id = x.id, sort = love.math.random()}'
		):Where('x => x.sort > 0.7'
		):OrderBy('x => x.sort')

	-- Grab the first
	local modId = q:First()

	if(modId ~= nil) then
		return modId.id
	end
	
	return nil

end
