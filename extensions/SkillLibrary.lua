--[[
Copyright 2016 MoikMellah

This file is part of the NoName sample module for use with the SYPHA Engine.

The NoName module is free software: you can redistribute it and/or modify it under the terms
of the CC-By-SA 4.0 license as published by Creative Commons.

This code is provided strictly as-in and WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the included file 'LICENSE.CC-BY-SA.txt' for full details.

You should have received a copy of the CC-By-SA 4.0 License along with
the NoName module. If not, see https://creativecommons.org/licenses/by-sa/4.0/
]]--

require('utils.xml')

-- Table declaration for class
SkillLibrary = {}

-- Metatable for class (to make SkillLibrary table the default lookup for class methods)
SkillLibrary_mt = {}
SkillLibrary_mt.__index = SkillLibrary

-- Constructor
function SkillLibrary.new()

	-- Make copy from template if provided, or default template if not
	local nActLib = {}

	-- Set metatable to get our class methods
	setmetatable(nActLib, SkillLibrary_mt)

	nActLib.skills = {}
	nActLib.count = 0

	nActLib.classes = {}

	return nActLib

end

function SkillLibrary:init()

	if self.skills == nil then self.skills = {} self.count = 0 end
	self.list = self.list or {}
	self.classes = self.classes or {}

end

function SkillLibrary:loadSet(s_fileName)

	--if(s_handle == nil) then return false end

	self:init()

	if(s_fileName == nil) then return false end

	if self.skills == nil then self.skills = {} end

	local aS = Xml.deserializeFile(s_fileName)

	for k,v in pairs(aS.skill) do
		self.skills[k] = v
	end

end

function SkillLibrary:loadClassInfo(s_fileName)
	--if(s_handle == nil) then return false end

	if(s_fileName == nil) then return false end

	if self.classes == nil then self.classes = {} end

	local aS = Xml.deserializeFile(s_fileName)

	for k,v in pairs(aS.class) do
		self.classes[k] = v
	end
end

function SkillLibrary:getClassName(class)

	local cls = self.classes[class] or {}
	
	return cls.displayName or 'Custom'

end

function SkillLibrary:getClassDescr(class)

	local cls = self.classes[class] or {}
	
	return cls.descr or ''

end
