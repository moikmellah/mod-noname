--[[
Copyright 2016 MoikMellah

This file is part of the NoName sample module for use with the SYPHA Engine.

The NoName module is free software: you can redistribute it and/or modify it under the terms
of the CC-By-SA 4.0 license as published by Creative Commons.

This code is provided strictly as-in and WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the included file 'LICENSE.CC-BY-SA.txt' for full details.

You should have received a copy of the CC-By-SA 4.0 License along with
the NoName module. If not, see https://creativecommons.org/licenses/by-sa/4.0/
]]--

require('classes.Actor')
require('utils.Inheritance')

MenuActor = Inherit(nil, Actor)

function MenuActor:initMenu()

	self.scene.vars.blockItemUI = true

	if(self.subMenu) then
		self.menuOptions = self:getSubMenu(self.subMenu)
	end

	if(self.menuOptions == nil) then
		return nil
	end
	
	local lgf = TextLibrary:setFont('default')
	local lw = 0

	local options = strToTable(self.menuOptions, '|')
	self.refs.optionList = {}
	
	self.vars.minOpt = #options
	
	for i = 1,#options do
		local k,v,d = strSplit(options[i],':')
		lw = math.max(lw, lgf:getWidth(k))
		self.prim[i+1] = {
			type='label', 
			visible = 1, 
			x = 0, 
			y = ((i-1) * 15), 
			text = k,
			color = {r='.6', g='.6', b='.6', a='1'}
		}
		self.refs.optionList[i] = {key = k, value = v, data = d}
		if(v ~= nil and v ~= '') then
			self.vars.minOpt = math.min(self.vars.minOpt,i)
		end
	end

	self.prim[1] = {
		type='rect', 
		visible = 1, 
		x = -5, 
		y = -5, 
		w = lw+10, 
		h = (#options*15)+5, 
		bkgColor = {r='.3', g='.3', b='1', a='1'},
		borderColor = {r='1', g='1', b='1', a='1'},
		borderWidth = 2
	}

	self.vars.cursorPos = self.vars.cursorPos or self.vars.minOpt
	
	self:setOptionColors()

end

function MenuActor:setOptionColors()
	
	self.vars.cursorPos = self.vars.cursorPos or 1	

	local options = self.refs.optionList

	for i = 1,#options do
		self.prim[i+1].color = self.prim[i+1].color or {a='1'}
		local cl = self.prim[i+1].color
		local c = .6
		if(i == self.vars.cursorPos) then
			c = 1
		end
		cl.r = c
		cl.g = c
		cl.b = c
	end
	
end

function MenuActor:updateCursor(dir)

	local d = dir or 1
	local options = self.refs.optionList
	
	self.vars.cursorPos = (self.vars.cursorPos or 1) + d
	self.vars.cursorPos = clamp(self.vars.minOpt,self.vars.cursorPos,#options)
	self:setOptionColors()

end

function MenuActor:execOption(arg)
	local cp = self.vars.cursorPos or 1
	local options = self.refs.optionList
	local selOpt = options[cp] or {}
	
	local action = self[selOpt.value]
	if(type(action) == 'function') then
		return action(self, selOpt.data, arg)
	end
end

function MenuActor:resumeGame()
	self.scene.env.timers.scene:set(1)
	if(self.refs.pcam ~= nil) then
		self.refs.pcam.refs.menu = nil
	end
	self.scene.vars.blockItemUI = false
	self.scene:remove(self)
end

function MenuActor:exitMenu()
	if(self.subMenu == 'gameplayOpts') then
		self.scene.env:savePrefs()
	end

	if(self.refs.parentMenu) then
		self.refs.parentMenu:pushEvent({type='FOCUS', subj=self, obj=self.refs.parentMenu})
	end

	if(self.vars.pauseScene) then
		self:resumeGame()
	else
		self.scene:remove(self)
	end
end

function MenuActor:quitGame()
	love.event.quit()
end

function MenuActor:exitToTitle()
	self.scene.env:requestScene(gconf.modulePath .. '/scenes/title.xml')
end

function MenuActor:toggleFullscreen()
	local newValue = (gconf.fullscreen == 'OFF')
	if(newValue) then
		gconf.fullscreen = 'ON'
	else
		gconf.fullscreen = 'OFF'
	end
	self.scene:setFullscreen(newValue)
	self:initMenu()
end

function MenuActor:toggleFiltering()
	local newValue = (gconf.filtering == 'OFF')
	if(newValue) then
		gconf.filtering = 'ON'
	else
		gconf.filtering = 'OFF'
	end
	self.scene.env:setFiltering(newValue)
	self:initMenu()
end

function MenuActor:getSubMenu(data)

	gconf.fullscreen = gconf.fullscreen or 'OFF'
	gconf.filtering = gconf.filtering or 'OFF'
	gconf.audioVolume = gconf.audioVolume or 70
	
	local diffStr = {[.25]='BEGINNER', [.5]='EASY', [.75]='NORMAL', [1]='HARD', [1.25]='INSANE'}
	
	local menus = {
		gameplayOpts = 'DIFFICULTY = '..diffStr[gconf.difficulty]..':updateNumericOption:difficulty,.25,1.25,.25'
			..'|FULLSCREEN = '..gconf.fullscreen..':toggleFullscreen'
			..'|FILTERING = '..gconf.filtering..':toggleFiltering'
			..'|VOLUME = '..(gconf.audioVolume..'%')..':updateNumericOption:audioVolume,0,100,10'
			..'|DEFADJ = '..gconf.defAdj..':updateNumericOption:defAdj,0.05,.95,.05'
			..'|DEBUG = '..gconf.debugVerbosity..':updateNumericOption:debugVerbosity,0,9,1',
		confirmQuit = 'Exit game?|NO:exitMenu|YES:quitGame',
		confirmExitTitle = 'Exit to title?|NO:exitMenu|YES:exitToTitle'
			
	}
	return menus[data]
end

function MenuActor:subMenuOptions(data)
	local subMenu = self:spawn('menuOption', self.x + 1, self.y + 1, self.z + 10)
	subMenu.subMenu = data
	subMenu.refs.parentMenu = self
	subMenu:initMenu()
	self.refs.subMenu = subMenu
	return 'menuOption.2'
end

function MenuActor:updateNumericOption(data, arg)
	
	local a = tonumber(arg) or 1

	local index,n,x,i = strSplit(data,',')
	
	local min = tonumber(n) or 0
	local max = tonumber(x) or 1000000
	local incr = tonumber(i) or 1

	gconf[index] = clamp(min,((gconf[index] or 0) + (a * incr)),max)
	self:initMenu()
	
end

function MenuActor:continueGame(data,arg)

	if(not(love.filesystem.exists('save1'))) then
		love.filesystem.createDirectory('save1')
	end
	
	local xp = 1
	
	if(data == 'newGamePlus' and love.filesystem.exists('save1/heroData.xml')) then
		local heroData = Xml.deserializeFile('save1/heroData.xml')
		xp = heroData.xp
	end
	
	if(not(love.filesystem.exists(gconf.modulePath .. '/scenes/A'))) then
		local seed = love.math.random(1,999999)
		local pcg = ProcGenLibrary.new(seed)

		local pathlen = gconf.pcgPathLen or 40
		local minbr = gconf.pcgMinBranch or 5
		local maxbr = gconf.pcgMaxBranch or 10
		local nodeChance = gconf.pcgNodeChance or .3
		local ascChance = gconf.pcgAscChance or .2
		local lvRate = gconf.pcgLevelRate or .2
		local initLv = math.ceil(xp/(gconf.expPerLvl or 3000))
	
		pcg:tryCreateDungeon(pathlen,minbr,maxbr,nodeChance,ascChance,lvRate,'A',initLv)
		debugPrint('Seed: '..seed, 2)
	end

	self:loadGameFromDisk()

end

function MenuActor:loadGameFromDisk()

	if(not(love.filesystem.exists('save1'))) then
		love.filesystem.createDirectory('save1')
	end

	local hero = self:spawn('defaultHero', 0, 0, 0)
	
	if(love.filesystem.exists('save1/heroData.xml')) then
		local heroData = Xml.deserializeFile('save1/heroData.xml')
		hero.inv = heroData.inv
		hero.inv.item = hero.inv.item or {}
		for k,v in pairs(hero.inv.item) do
			v.id = nil
		end
		hero.classTemplate = heroData.classList[1]..'-template'
		hero.base = nil
		hero:rollStats()
		hero.base.classList = heroData.classList or {}
		hero.base.skills = heroData.skills or {}
		hero:setSkillBuffs()
		hero:setEquipmentBuffs()
		hero.base.xp = heroData.xp
	else
		-- New char, for now
		hero.base.classList = {[1] = 'warrior', [2] = 'mage'}
		hero.classTemplate = 'warrior-template'
	end

	self.scene.env.timers.scene:set(1)
	hero:rollStats()

	local sceneFile = sceneFile or self.scene.relPath .. '/A/1.xml'
	
	if(love.filesystem.exists('save1/gameData.xml')) then
		local gameData = Xml.deserializeFile('save1/gameData.xml')
		self.scene.env.flags = gameData.flags or {}
		sceneFile = gameData.scene
	else
		self.scene.env.flags = {}
	end

	local mapFile = 'save1/mapData.xml'
	if(not(love.filesystem.exists(mapFile))) then
		mapFile = nil
	end
	local mapLib = MinimapLibrary.new()
	mapLib:loadMapData(mapFile)
	self.scene.env.vars.mapLib = mapLib
	self.scene.env.sceneList = {}
	
	hero:teleport(units(384), units(286), nil, sceneFile)
	
end

function MenuActor:newGame(data,arg)

	local paths = {
		[1] = pathToFile(gconf.modulePath .. '/scenes/A/'),
		[2] = pathToFile(gconf.modulePath .. '/maps/A/'),
		[3] = '/save1/gameData.xml',
		[4] = '/save1/mapData.xml'
	}
	
	if(data ~= 'newGamePlus') then
		paths[5] = '/save1/heroData.xml'
	end

	-- Remove old dungeon and start over
	for kp,path in ipairs(paths) do
		debugPrint('Removing path '..path, 4)
		if(love.filesystem.exists(path)) then

			local fullList = love.filesystem.getDirectoryItems(path)
			for k,v in ipairs(fullList) do
				if(not(love.filesystem.remove(path .. '/' .. v))) then
					debugPrint('NOREMOVE ' .. path .. '/' .. v, 4)
				else
					debugPrint('Removed ' .. path .. '/' .. v, 4)
				end
			end
		
		end
		if(not(love.filesystem.remove(path))) then
			debugPrint('NOREMOVE ' .. path, 4)
		end
	
	end
	
	self:continueGame(data,arg)
	--self:loadGameFromDisk()

end

function MenuActor:spawnMap(data,arg)

	local subMenu = self:spawn('menuOption', self.x + 1, self.y + 1, self.z + 10)
	subMenu.menuOptions = "MAP"
	subMenu.refs.parentMenu = self
	subMenu:initMenu()
	self.refs.subMenu = subMenu
	
	local pr = {}
	pr.type = 'image'
	pr.visible = 1
	pr.img = self.scene.env.vars.mapLib:composeMap(true, 8, 6, nil, true)
	pr.x = -(pr.img:getWidth()/2)
	pr.y = -(pr.img:getHeight()/2)
	
	subMenu.prim = subMenu.prim or {}
	table.insert(subMenu.prim, pr)
	
	local outline = {}
	outline.type = 'label'
	outline.visible = 1
	outline.x = pr.x
	outline.y = pr.y
	outline.w = pr.img:getWidth()
	outline.h = pr.img:getHeight()
	outline.borderColor  = {r='1', g='1', b='1', a='1'}
	outline.borderWidth = 2
	table.insert(subMenu.prim, outline)
		
	return 'menuOption.2'

end

function MenuActor:notifyListener(data,arg)

	local listener = self.refs.listener
	if(listener == nil) then return nil end

	local ev = {}
	ev.type = 'MENU-INPUT'
	ev.obj = data
	
	self.refs.listener:pushEvent(ev)
end
