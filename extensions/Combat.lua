--[[
Copyright 2016 MoikMellah

This file is part of the NoName sample module for use with the SYPHA Engine.

The NoName module is free software: you can redistribute it and/or modify it under the terms
of the CC-By-SA 4.0 license as published by Creative Commons.

This code is provided strictly as-in and WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the included file 'LICENSE.CC-BY-SA.txt' for full details.

You should have received a copy of the CC-By-SA 4.0 License along with
the NoName module. If not, see https://creativecommons.org/licenses/by-sa/4.0/
]]--

function Actor:calcDamage(t_obj)

	if(t_obj == nil) then return nil end

	if(t_obj.vars.dmg == nil) then return nil end

	-- First, check whether t_obj has a parent
	local p_obj = t_obj.refs.parentObj or t_obj

	local totalDmg = 0
	local totalKb = 0

	local physDmgTypes = "bash|slash|pierce"
	local magDmgTypes = "fire|earth|lightning|water|wind|darkness|holy|heal|poison"

	-- Loop through the damage types in order
	for k,v in pairs(t_obj.vars.dmg) do

		-- Physical damage
		if(string.match(physDmgTypes, k)) then

			local catDmg = v

			local mult = 1

			-- Apply multiplier(s?)
			local p_curr = p_obj.current or {}
			mult = mult + (p_obj.current['dmg' .. k] or 0)

			local t_attrs = strToTable(t_obj.attrs, ',')
			for ak,av in ipairs(t_attrs) do
				mult = mult + (p_obj.current['dmg' .. av] or 0)
			end

			-- Multiply
			catDmg = catDmg * mult

			-- Add str
			catDmg = catDmg + p_obj.current.str

		end

		-- Magic damage

	end

end



function Actor:simpleDamage(t_obj)

	if(t_obj == nil) then return nil end

	t_obj.attack = t_obj.attack or {}

	if((t_obj.attack.dmg or t_obj.vars.dmg) == nil) then return nil end

	-- First, check whether t_obj has a parent
	local p_obj = t_obj.refs.parentObj or t_obj

	local totalDmg = t_obj.attack.dmg or t_obj.vars.dmg or 0
	local finalDmg = 0

	local tmpDef = 0
	local tmpKres = 1
	local defAdj = gconf.defAdj or .9

	local euler = 2.718

	local atkmult = 1

	local critmult = 1

	local magDmgTypes = "fire|earth|lightning|water|wind|darkness|holy|heal|poison"

	local physDefMult = 1
	local magDefMult = 1	

	-- Apply multiplier(s?)
	local p_curr = p_obj.current or {}
	
	local colStr = 'default'
	
	local t_attrs = strToTable(t_obj.attrs or '', ',')
	local attrDone = {}
	
	for ak,av in ipairs(t_attrs) do
		--atkmult = atkmult + (p_curr['dmg' .. av] or 0)

		if(attrDone[av] == nil) then
			if(string.match(magDmgTypes, av)) then
				magDefMult = magDefMult - (self.current['res' .. av] or 0)
				--debugPrint('res'.. av ..':'..(self.current['res'..av] or 0))

				colStr = av
			else
				physDefMult = math.max(physDefMult - (self.current['res' .. av] or 0),0)
			end
			attrDone[av] = true
		end
	end

	if(t_obj.objType == 'magic') then
		tmpDef = self.current.mdef or 0
		if(string.find(self.action, 'BLOCK') ~= nil) then
			tmpDef = tmpDef + self.current.blockMdef
			tmpKres = 2
		end
	elseif(t_obj.objType == 'projectile') then
		totalDmg = totalDmg + (p_curr.agi or 0)
		tmpDef = self.current.def or 0
		if(string.find(self.action, 'BLOCK') ~= nil) then
			tmpDef = tmpDef + self.current.blockDef
			tmpKres = 2
		end
	else --if(t_obj.objType == 'melee') then
		totalDmg = totalDmg + (p_curr.str or 0)
		tmpDef = self.current.def or 0
		if(string.find(self.action, 'BLOCK') ~= nil) then
			tmpDef = tmpDef + self.current.blockDef
			tmpKres = 2
		end
	end

	local critChance = (euler ^ (2.5 * -(self.current.agi or 0) / (p_curr.agi or 1)))

	-- Find out if it's a CRIT
	if(love.math.random() < critChance) then
		critmult = 2
		--debugPrint(critChance)
	end

	-- Apply attack multiplier first
	totalDmg = (totalDmg * atkmult)

	-- Apply raw defense value
	finalDmg = totalDmg * (euler ^ (defAdj * -tmpDef / math.abs(totalDmg)))

	local hits = t_obj.attack.hits or t_obj.vars.hits or 1

	-- Apply defense multiplier last
	finalDmg = math.ceil((finalDmg * physDefMult * magDefMult * critmult) / hits)

	if(type(t_obj.attack.buff) == 'table') then

		for k,v in pairs(t_obj.attack.buff) do

			local bChance = v.chance or 1

			local tChance = bChance * (euler ^ (defAdj * -(self.current.lck or 1) / (p_obj.current.lck or 1)))

			tChance = tChance * critmult

			if(love.math.random() < tChance) then
				self:addBuff(v, t_obj.id .. '-' .. k)
			end

		end

	end
	
	local dCol = gconf.dmgcolor[colStr]
	
	local tCol = tableCopy(dCol)
	
	-- Apply stunTtl, which determines how long char has temp invincibility after being hit
	local stunmult = math.min(self.timer:dtMult(), (self.timer:dtMult() / p_obj.timer:dtMult()))
	self.vars.stunTtl = (t_obj.attack.stun or .33) * stunmult
	if(self.vars.stunTtl > 0) then
		self.vars.color = tCol --{r=1,g=1,b=1,a=.2,shader='negTint',ttl=.1}
	end

	local knockback = (t_obj.attack.knockback or t_obj.vars.knockback or 7) / tmpKres

	knockback = knockback * (1 + (p_curr.dmgknockback or 0))
	knockback = knockback * (1 - (self.current.resknockback or 0))
	if(critmult > 1) then knockback = knockback * 1.5 end

	knockback = math.max(0, knockback)

	-- No knockback if damage is below a threshold (account for pathetic attacks)
	if(finalDmg <= (self.current.maxhp or 1)*(self.scene.env.conf.knockBackDmgImmune or .01)) then knockback = 0 end

	if(p_obj.objType == 'actor') then
		if(self.refs.tattle ~= nil) then
			local tDist = math.abs(self.refs.tattle:getX() - self:getX())
			local pDist = math.abs(p_obj:getX() - self:getX())
			if(pDist < tDist) then self.refs.tattle = p_obj end
		else
			self.refs.tattle = self.refs.tattle or p_obj
		end
	end

	return finalDmg, knockback, critmult

end

-- Standard damage/collision handler
function Actor:handleDamage(that)

	that.obj.attack = that.obj.attack or {}
	if((that.obj.attack.dmg or that.obj.vars.dmg or 0) == 0) then return nil end
	if((self.vars.stunTtl or 0) > 0) then return nil end
	
	local frm = self:getFrame()
	
	if((frm ~= nil) and (frm.invc or 0) > 0) then return nil end
	
	self.scene:pushEvent({type="ACTOR-DMG", subj=self})
	local dmg = 0
	local dnkb = 0
	local crit = 0
	dmg,dnkb,crit = self:simpleDamage(that.obj)

	self.current.hp = math.max((self.current.hp or 0) - (dmg or 0), 0)

	local dmgLbl = self:spawn('bounceLabel', (that.zone[0]+that.zone[2])/2, (that.zone[1]+that.zone[3])/2, self.z + .5)
	local splat = self:spawn('bloodSplatter', (that.zone[0]+that.zone[2])/2, (that.zone[1]+that.zone[3])/2, self.z + .5)

	local zCtr = (that.zone[0]+that.zone[2])/2
	local meZone = self:getBounds()
	local mCtr = (meZone[0]+meZone[2])/2

	local zFacing = that.obj.facing
	if(that.obj.objType == 'actor') then
		if(zCtr > mCtr) then 
			zFacing = -1
		elseif(zCtr < mCtr) then
			zFacing = 1
		end
	end

	dmgLbl.prim[1].text = dmg
	if(crit > 1) then dmgLbl.sx = 1.5 dmgLbl.sy = 1.5 end

	splat.xv = 7 * zFacing
	splat.facing = zFacing
	if(type(self.vars.bloodColor) == 'table') then
		splat.prim[1].color = tableCopy(self.vars.bloodColor)
	end
	self.xv = dnkb * zFacing

	if(self.current.hp <= 0) then
		self:grantXp(that.obj)
	end
	return dnkb

end

function Actor:getDamageAction(n_dnkb, that)

	local dmgAction = nil

	if((self.grounded == 1) and 
		((type(that) ~= 'table') or 
		(that.dir ~= Actor.DIRBITS.down))) then
		
		if((n_dnkb or 0) >= 10 or self.current.hp <= 0) then

			self.yv = -8
			dmgAction = 'DMG-AIR.1'

		elseif(string.find(self.action, 'BLOCK') ~= nil) then
			dmgAction = self.action
		else

			-- crouching first
			if(string.match(self.action, 'CRC') or string.match(self.action, 'CROUCH')) then

				dmgAction = 'DMG-CRC.1'

			else

				dmgAction = 'DMG.1'

			end

		end

	else

		self.yv = -8
		dmgAction = 'DMG-AIR.1'

	end

	return dmgAction

end

function Actor:getDamageActionShort(n_dnkb, that)

	local dmgAction = nil

	if((self.grounded == 1) and 
		((type(that) ~= 'table') or 
		(that.dir ~= Actor.DIRBITS.down))) then

		if((n_dnkb or 0) >= 10 or self.current.hp <= 0) then

			self.yv = -8
			dmgAction = 'DMG-AIR.1'

		else

			dmgAction = 'DMG.1'

		end

	else

		self.yv = -8
		dmgAction = 'DMG-AIR.1'

	end

	return dmgAction

end
