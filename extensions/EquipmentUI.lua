--[[
Copyright 2016 MoikMellah

This file is part of the NoName sample module for use with the SYPHA Engine.

The NoName module is free software: you can redistribute it and/or modify it under the terms
of the CC-By-SA 4.0 license as published by Creative Commons.

This code is provided strictly as-in and WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the included file 'LICENSE.CC-BY-SA.txt' for full details.

You should have received a copy of the CC-By-SA 4.0 License along with
the NoName module. If not, see https://creativecommons.org/licenses/by-sa/4.0/
]]--

function Actor:linkGuiElements()

	local guiQuery = Queryable.new(self.scene.vars.guiArray)
	
	for k,v in ipairs(self.scene.vars.guiArray) do
		local x = v.x
		local y = v.y
		
		local downElement = guiQuery:Where('x => x.y > '..y):OrderBy('x => dist(x.x, x.y, '..x..', '..y..')'):First()
		if(downElement) then v.d = downElement.invPos end

		local upElement = guiQuery:Where('x => x.y < '..y):OrderBy('x => dist(x.x, x.y, '..x..', '..y..')'):First()
		if(upElement) then v.u = upElement.invPos end
		
		local leftElement = guiQuery:Where('x => x.x < '..x):OrderBy('x => dist(x.x, x.y, '..x..', '..y..')'):First()
		if(leftElement) then v.l = leftElement.invPos end

		local rightElement = guiQuery:Where('x => x.x > '..x):OrderBy('x => dist(x.x, x.y, '..x..', '..y..')'):First()
		if(rightElement) then v.r = rightElement.invPos end
	end
end

function Scene:setupInventoryGui()

	local invMax = gconf.invMax or 61

	self.vars.sourceActor = self.env.vars.hero
	self.vars.guiArray = {}
	--[[
	for i = 1,64 do
		local j = i-1
		local thing = self:spawn('invIcon', 2+(1.5*math.floor(j/4)), 2+(1.5*math.floor(j%4)), 0)
		thing.invPos = i
		self.vars.guiArray[i] = thing
	end
	]]--
	
	-- Action slots
	for i = 1,3 do
		local j = i-1
		local thing = self:spawn('invIcon', 5+(1.5*j), 3.5, 0)
		thing.invPos = i
		thing.slot = 'act'
		self.vars.guiArray[i] = thing
	end
	
	-- Equipment slots
	for i = 4,9 do
		local j = i-4
		local thing = self:spawn('invIcon', 15+(1.5*j), 3.5, 0)
		thing.invPos = i
		self.vars.guiArray[i] = thing
	end
	self.vars.guiArray[4].slot = 'shield'
	self.vars.guiArray[5].slot = 'armor'
	self.vars.guiArray[6].slot = 'helm'
	self.vars.guiArray[7].slot = 'boot'
	self.vars.guiArray[8].slot = 'accessory'
	self.vars.guiArray[9].slot = 'accessory'
	
	-- Ammo slots
	for i = 10,13 do
		local j = i-10
		local thing = self:spawn('invIcon', 5, 7.5+(1.5*j), 0)
		thing.invPos = i
		thing.slot = 'ammo'
		self.vars.guiArray[i] = thing
	end
	
	-- Inventory slots
	for i = 14,invMax do
		local j = i-14
		local ix = 8+(1.5*math.floor(j/4))
		local iy = 7.5+(1.5*math.floor(j%4))
		local thing = self:spawn('invIcon', ix,iy, 0)
		thing.invPos = i
		thing.slot = 'inv'
		self.vars.guiArray[i] = thing
	end
	
	-- Other elements
	
	-- Cursor obj
	local curs =  self.vars.curs or self:spawn('invCursor',0,0)
	curs:linkGuiElements()
	for i = 4,6 do
		curs.prim[i].visible = 1
	end
	curs.prim[10].visible = 0
	
	--Cleanup
	for i = 1,#self.vars.guiArray do
		self.vars.guiArray[i]:setSingleInvIcon()
	end
end

function Scene:setupSkillGui()
	self.vars.sourceActor = self.env.vars.hero
	self.vars.guiArray = {}
	--[[
	for i = 1,64 do
		local j = i-1
		local thing = self:spawn('invIcon', 2+(1.5*math.floor(j/4)), 2+(1.5*math.floor(j%4)), 0)
		thing.invPos = i
		self.vars.guiArray[i] = thing
	end
	]]--
	
	-- Action slots
	for i = 1,3 do
		local j = i-1
		local thing = self:spawn('invIcon', 5+(1.5*j), 3.5, 0)
		thing.slot = 'act'
		table.insert(self.vars.guiArray,thing)
		thing.invPos = #self.vars.guiArray
	end

	local skillArray = self.env.skillLib.skills
	
	local skills = Queryable.new(skillArray):Where('x => contains(x.class,"warrior") ~= nil'
				):OrderBy('x => x.displayName'
				):ToArray()
	
	-- Skill slots
	for i = 1,#skills do
		local j = i-1
		local ix = 5+(3*math.floor(j/2))
		local iy = 7.5+(3*math.floor(j%2))
		local thing = self:spawn('invIcon', ix,iy, 0)
		thing.skillName = skills[i].name
		thing.slot = 'skill'
		table.insert(self.vars.guiArray,thing)
		thing.invPos = #self.vars.guiArray
	end
	
	-- Navigation
	for i = 1,2 do
		local j = i-1
		local thing = self:spawn('invIcon', 2, 7.5+(1.5*j), 0)
		thing.slot = 'inv'
		table.insert(self.vars.guiArray,thing)
		thing.invPos = #self.vars.guiArray
	end
	
	-- Equipment slots
--[[	for i = 4,9 do
		local j = i-4
		local thing = self:spawn('invIcon', 15+(1.5*j), 3.5, 0)
		thing.invPos = i
		self.vars.guiArray[i] = thing
	end
	self.vars.guiArray[4].slot = 'shield'
	self.vars.guiArray[5].slot = 'armor'
	self.vars.guiArray[6].slot = 'helm'
	self.vars.guiArray[7].slot = 'boot'
	self.vars.guiArray[8].slot = 'accessory'
	self.vars.guiArray[9].slot = 'accessory'
	
	-- Ammo slots
	for i = 10,13 do
		local j = i-10
		local thing = self:spawn('invIcon', 2, 7.5+(1.5*j), 0)
		thing.invPos = i
		thing.slot = 'ammo'
		self.vars.guiArray[i] = thing
	end
	
	-- Inventory slots
	for i = 14,invMax do
		local j = i-14
		local ix = 5+(1.5*math.floor(j/4))
		local iy = 7.5+(1.5*math.floor(j%4))
		local thing = self:spawn('invIcon', ix,iy, 0)
		thing.invPos = i
		thing.slot = 'inv'
		self.vars.guiArray[i] = thing
	end
]]--	
	-- Other elements
	
	-- Cursor obj
	local curs =  self.vars.curs or self:spawn('invCursor',0,0)
	curs:linkGuiElements()
	for i = 4,6 do
		curs.prim[i].visible = 0
	end
	curs.prim[10].visible = 1
	
	--Cleanup
	for i = 1,#self.vars.guiArray do
		self.vars.guiArray[i]:setSingleInvIcon()
	end
end
