--[[
Copyright 2016 MoikMellah

This file is part of the NoName sample module for use with the SYPHA Engine.

The NoName module is free software: you can redistribute it and/or modify it under the terms
of the CC-By-SA 4.0 license as published by Creative Commons.

This code is provided strictly as-in and WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the included file 'LICENSE.CC-BY-SA.txt' for full details.

You should have received a copy of the CC-By-SA 4.0 License along with
the NoName module. If not, see https://creativecommons.org/licenses/by-sa/4.0/
]]--

function Actor:rollStats()

	local clsBase = self.scene.env.actorLib.actors[self.classTemplate]

	if(clsBase ~= nil and self.base == nil) then
		self.base = tableCopy(clsBase.base)
		if(bit.band((self.faction or 0), 1) ~= 1) then
			local difficulty = gconf.difficulty or .75
			for k,v in pairs(self.base) do
				if (k ~= 'hp' and k ~= 'mp' and type(v) == 'number') then
					self.base[k] = v * difficulty
				end
			end
		end
	end

	self.base = self.base or {}

	self.current = self.current or {}

	self.vars.defaultColor = nil
	
	local origLevel = self.current.level or 1

	local origHpRatio = (self.current.hp or -1) / (self.current.maxhp or 1)
	if(origHpRatio < 0) then origHpRatio = nil end
	local origMpRatio = (self.current.mp or -1) / (self.current.maxmp or 1)
	if(origMpRatio < 0) then origMpRatio = nil end

	-- First, establish level
	if(self.hero == 1) then 
		self.base.xp = self.base.xp or 1 
	else
		self.base.xp = self.base.xp or ((self.scene.vars.level or 1)-1) * (gconf.expPerLvl or 3000) + 1	
	end
	self.base.xp = self.base.xp or ((gconf.startLvl-1) * gconf.expPerLvl)

	-- Clear current stats
	for k,v in pairs(self.current) do
		if(type(v) == 'number') then
			self.current[k] = 0
		end
	end

	local expPerLvl = gconf.expPerLvl or 3000

	local maxLvl = gconf.maxLvl or 100

	self.current.level = math.min(math.max(math.floor(self.base.xp / expPerLvl) + 1, 1),maxLvl)

	local clv = self.current.level -- + (self.scene.env.conf.lvAdjust or 9)
	local fct = self:getStatMultiplier(clv)

	-- Next, stats
	self.current.maxhp = math.ceil(self.base.hp * fct)
	self.current.maxmp = math.ceil(self.base.mp * fct)

	self.current.str = math.ceil(self.base.str * fct)
	self.current.con = math.ceil(self.base.con * fct)
	self.current.int = math.ceil(self.base.int * fct)
	self.current.wis = math.ceil(self.base.wis * fct)
	self.current.agi = math.ceil(self.base.agi * fct)
	self.current.lck = math.ceil(self.base.lck * fct)

	self.current.walkspeed = self.scene.env.conf.baseWalkSpeed or 4.5

	-- Apply stat buffs (positive and negative)
	self.base.buff = self.base.buff or {}
	for k,v in pairs(self.base.buff) do
		self:applyBuff(v)
	end
	
	-- Apply/evaluate skills
	self.current.skillBuffs = self.current.skillBuffs or {}
	for k,v in pairs(self.current.skillBuffs) do
		self:applyBuff(v)
	end

	self.current.tempBuffs = self.current.tempBuffs or {}
	for k,v in pairs(self.current.tempBuffs) do
		self:applyBuff(v)
	end

	self.current.eqBuffs = self.current.eqBuffs or {}
	for k,v in pairs(self.current.eqBuffs) do
		self:applyBuff(v)
	end

	self.current.def = math.max(self.current.con, 0)
	self.current.blockDef = self.current.def
	self.current.mdef = math.max(self.current.wis, 0)
	self.current.blockMdef = self.current.mdef

	self.inv = self.inv or {}
	self.inv.item = self.inv.item or {}
	-- Apply equipment raw def values
	for k = 1,9 do
		local v = self.inv.item[k]
		if(v ~= nil and v.itemId ~= nil) then
			local itemId = v.itemId
			local item = self.scene.env.itemLib.items[itemId]
			local itemDef = 0 --item.equip.def or 0
			local itemMdef = 0 --item.equip.mdef or 0
			local itemMult = 1

			if(type(item.equip) == 'table') then
				itemDef = item.equip.def or 0
				itemMdef = item.equip.mdef or 0
			end

			local s_attrs = item.attrs
			
			if(type(v.mod) == 'table') then
				for k = 1,3 do
				
					local mid = v.mod[k]
					
					local m = self.scene.env.modLib.mods[mid]
				
					if(type(m) == 'table') then
						itemMult = itemMult + (m.mult or 0)
						if(type(m.attrs) == 'string') then
							s_attrs = s_attrs .. ',' .. m.attrs
						end
					end

				end
			end
			
			local t_attrs = strToTable(s_attrs or '', ',')
			
			local defmult = 1
	
			local attrdone = {}
	
			for ak,av in ipairs(t_attrs) do
				if(attrdone[av] == nil) then defmult = defmult + (self.current['dmg' .. av] or 0) attrdone[av] = true end
			end

			itemDef = math.max(itemDef * itemMult, 0)
	
			itemDef = math.max((itemDef or 0) * defmult, 0)
			
			-- Handle shields differently
			if(k ~= 4) then
				self.current.def = self.current.def + math.max(math.ceil(itemDef), 0)
				self.current.mdef = self.current.mdef + math.max(math.ceil(itemMdef * itemMult), 0)
			else
				self.current.blockDef = self.current.blockDef + math.max(math.ceil(itemDef), 0)
				self.current.blockMdef = self.current.blockMdef + math.max(math.ceil(itemMdef * itemMult), 0)
			end
		end
	end

	-- All non-res/dmg stats should be min 1
	for k,v in pairs(self.current) do

		if(type(v) == 'number' and not(string.match(k, '^res')) and not(string.match(k, '^dmg'))) then
			self.current[k] = math.max(self.current[k], 1)
		end

	end

	-- Make sure HP is populated
	self.current.hp = (origHpRatio or 1) * self.current.maxhp
	self.current.mp = (origMpRatio or 1) * self.current.maxmp

end

function Actor:getStatMultiplier(lv)

	return Actor.getStatMultExpOffset(lv)

end

function Actor.getStatMultOrig(lv)

	local clv = lv or 1
	local growthRate = (gconf.growthRate or 1.025)
	local fct = (growthRate ^ (clv - 1)) * clv
	return fct

end

function Actor.getStatMultExpOffset(lv)

	local growthRate = gconf.growthRate or 1.03
	local lin = gconf.statOffset or -40
	local lofs = gconf.statLvlOffset or 40
	local mult = gconf.statMultiplier or 20

	local z = (lv or 1) + lofs
	
	local fct = ((growthRate ^ z) * mult) + lin
	
	return fct

end

function Actor:ageBuffs()

	local expiredBuff = false

	self.current = self.current or {}

	self.current.tempBuffs = self.current.tempBuffs or {}

	for k,v in pairs(self.current.tempBuffs) do
		v.ttl = (v.ttl or 0) - self.dt
		if(v.ttl <= 0) then
			self.current.tempBuffs[k] = nil
			expiredBuff = true
		end
	end

	if(expiredBuff) then self:rollStats() end

	return true

end

function Actor:addBuff(t_buff)

	if(t_buff == nil) then return false end

	self.current = self.current or {}
	self.current.tempBuffs = self.current.tempBuffs or {}

	local n_buff = tableCopy(t_buff)

	-- If it's not timed, just apply it now (for effect scripts, etc.)
	if(type(n_buff.ttl) == 'number') then
		--debugPrint(n_buff)
		self.current.tempBuffs[n_buff.name] = tableCopy(n_buff)
		self:rollStats()
	else
		self:applyBuff(n_buff) 
	end

	return true

end

function Actor:applyBuff(v)

	if(type(v) ~= 'table') then return nil end

	--debugPrint('Applying buff ' .. (v.name or '[generic]') .. ' Lv ' .. (v.buffLvl or 1))

	local buffMult = Actor:getStatMultiplier(v.buffLvl or 1)
	if(v.flat == 1) then 
		buffMult = 1 
	elseif(v.flatmult == 1) then
		buffMult = v.buffLvl or 1
	end

	for bk,bv in pairs(v) do
		if(bk == 'script') then
			if(type(v[bk]) == 'string') then
				v[bk] = loadstring('local self,that = ... '.. bv)
			end
			if(type(v[bk]) == 'function') then
				self.tmpScr = v[bk]
				self:tmpScr(v)
			end
		else
			local buffAmt = ((tonumber(bv) or 0) * buffMult)
			if(v.flat ~= 1 and v.flatmult ~= 1) then buffAmt = math.ceil(buffAmt) end
			self.current[bk] = (self.current[bk] or 0) + buffAmt
			--debugPrint('Buffamt (' .. bk .. '): ' .. buffAmt)
		end
	end

end

function Actor:setEquipmentBuffs()

	self.current = self.current or {}

	-- Reset.  It's okay this time, as it won't be too often (just when changing equipment).
	self.current.eqBuffs = {}

	self.inv = self.inv or {}
	self.inv.item = self.inv.item or {}

	for k = 1,9 do
		local v = self.inv.item[k]
		if(v ~= nil) then
			local itemId = v.itemId
			local item = self.scene.env.itemLib.items[itemId]
			if(item ~= nil and type(item.buff) == 'table') then
				for bk,bv in ipairs(item.buff) do
					local cpbv = tableCopy(bv)
					cpbv.buffLvl = v.level or 1
					table.insert(self.current.eqBuffs, bv)
				end
			elseif(item == nil) then
				--debugPrint('Missing itemtype ' .. itemId)
			end
			
			if(type(v.mod) == 'table') then
				for k = 1,3 do
				
					local mid = v.mod[k]
					
					local m = self.scene.env.modLib.mods[mid]
					
					if(type(m) == 'table' and type(m.equip) == 'table' and type(m.equip.buff) == 'table') then
						for bk,bv in ipairs(m.equip.buff) do
							local cpbv = tableCopy(bv)
							cpbv.buffLvl = v.level or 1
							table.insert(self.current.eqBuffs, cpbv)
						end
					end

				end
			end
		end
	end

	return true

end

function Actor:setSkillBuffs()

	if(self.base == nil) then return end

	self.current = self.current or {}

	-- Reset.  It's okay this time, as it won't be too often (just when changing equipment).
	self.current.skillBuffs = {}

	self.base.skills = self.base.skills or {}

	for k,v in pairs(self.base.skills) do
		if(v ~= nil) then
			local skillId = k
			local item = self.scene.env.skillLib.skills[skillId]
			if(item ~= nil and type(item.buff) == 'table') then
				for bk,bv in ipairs(item.buff) do
					local cpbv = tableCopy(bv)
					cpbv.buffLvl = v.level or 1
					table.insert(self.current.skillBuffs, cpbv)
				end
			elseif(item == nil) then
				--debugPrint('Missing itemtype ' .. itemId)
			end
		end
	end

	return true

end

function Actor:updateCounters()

	self.vars.stunTtl = self.vars.stunTtl or 0

	if(self.vars.stunTtl > 0) then 
		self.vars.stunTtl = math.max(self.vars.stunTtl - self.dt, 0) 
	end

	self.vars.color = self.vars.color or self.vars.defaultColor

	if(type(self.vars.color) == 'table') then
		if(type(self.vars.color.ttl) == 'number') then
			self.vars.color.ttl = self.vars.color.ttl - self.dt
			if(self.vars.color.ttl < 0) then
				self.vars.color = self.vars.defaultColor
			end
		else
			self.vars.color = self.vars.defaultColor
		end
	end

	local mpPerSec = (self.current.maxmp or 0) / (gconf.mpRegenTime or 120)
	self.current.mp = math.min((self.current.mp or 0) + (mpPerSec * self.dt), (self.current.maxmp or 0))
	
	self:ageBuffs()

	self:spawnDropItems()

end

function Actor:getWalkspeed()

	return self.current.walkspeed * ((self.vars.dashing or 0) + 1) * self.facing

end

function Actor:checkFallthru()
	local mycam = self.camera or -1
	
	local b = self:getBounds()

	local y = math.floor(b[3]) + 1
	
	local retBool = true
	
	for x = math.floor(b[0]),math.floor(b[2]) do
		local t = self.scene:getTile(mycam, x, y)
		if(t ~= nil) then
			local coll = tonumber(t.coll) or 0
			if(bit.band(coll, 14) ~= 0) then retBool = false end
		end
	end
	
	return retBool
end

function Actor:grantXp(t_obj)
	
	local p_obj = t_obj.refs.parentObj or t_obj

	-- Get top-level object
	while((p_obj.refs ~= nil) and (p_obj.refs.parentObj ~= nil)) do
		p_obj = p_obj.refs.parentObj
	end	

	local curLev = p_obj.current.level or 1

	local diff = (self.current.level or 1) - curLev
	
	local expPerLvl = gconf.expPerLvl or 3000
	local maxLvl = gconf.maxLvl or 100
	p_obj.base.xp = math.ceil(p_obj.base.xp + (100*((gconf.expAdjust or 1.2)^diff)))
	local newLevel = math.min(math.max(math.floor(p_obj.base.xp / expPerLvl) + 1, 1),maxLvl)

	-- Level up!
	if(newLevel ~= curLev) then
		-- Grant max hp on level up?
		p_obj.current.hp = p_obj.current.maxhp
		
		p_obj:rollStats()
		
		local b = p_obj:getBounds()
		
		local dmgLbl = p_obj:spawn('bounceLabel', (b[0]+b[2])/2, b[1], p_obj.z + .5)
		
		dmgLbl.prim[1].text = 'LEVEL UP!'
	end
	
end

function Actor:availableSp()

	self.base.skills = self.base.skills or {}
	
	local skillLib = self.scene.env.skillLib
	
--[[	local levels = Queryable.new(self.base.skills
			):Where('x => (x.level or 0) > 0'
--			):Select('x => x.level'
			):ToArray()
]]--			
	local used = 0
	for k,v in pairs(self.base.skills) do
		local skillDef = skillLib.skills[k]
		local cost = skillDef.cost or 1
		used = used + ((v.level or 0) * cost)
	end
	
	return math.max(self.current.level - used, 0)

end
